\subsection{Experimentos sobre $x_0$}
\label{sec:resultados-x0}
A continuación se presentan gráficos mostrando el comportamiento de las sucesiones generadas por cada método para distintos $x_0$. Cada curva representa a $x_n$ en función de $n$ para un determinado $x_0$. En el caso del método de bisección, $x_0$ es el extremo superior del intervalo inicial, ya que como se dijo en la sección \ref{sec:desarrollo-aplicacion-metodos-problema}, el extremo inferior es fijo.

En primer lugar, se incluyen casos típicos de buen comportamiento de los métodos (figura (\ref{fig:xnvsn-muestra-5})).

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-5-Biseccion-funcion-F-prec--50-tol--10e11.pdf}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-5-Newton-funcion-F-prec--50-tol--10e11.pdf}
\end{center}
\caption{Muestra 5. $x_n$ en función de $n$ para distintos $x_0$. Métodos de bisección y Newton (izquierda, derecha) utilizando la función objetivo $F_X(x)$, con nivel de precisión alto.}
\label{fig:xnvsn-muestra-5}
\end{figure}

Los gráficos obtenidos utilizando $L_X(x)$ se omiten para el caso previo porque son muy similares. Para la muestra $3$ (figura (\ref{fig:xnvsn-muestra-3})) sí se observan diferencias entre tomar una función o la otra al aplicar el método de Newton:

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-3-Newton-funcion-L-prec--50-tol--10e11.pdf} 
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-3-Newton-funcion-F-prec--50-tol--10e11.pdf} 
\end{center}
\caption{Muestra 3. $x_n$ en función de $n$ para distintos $x_0$. Método de Newton, utilizando ambas funciones (izquierda $L_X(x)$, derecha $F_X(x)$), con nivel de precisión alto.}
\label{fig:xnvsn-muestra-3}
\end{figure}
 
Todos los gráficos obtenidos para el método de bisección muestran una estructura similar, independientemente de la función utilizada; en la figura (\ref{fig:xnvsn-muestra-9}) se muestra un ejemlo. Los valores de $x_0$ para los cuales el método no convergió a $\tilde\beta$ se marcan con una cruz.

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-9-Biseccion-funcion-L-prec--50-tol--10e11.pdf} 
\end{center}
\caption{Muestra 9. $x_n$ en función de $n$ para distintos $x_0$. Método de bisección, utilizando la función $L_X(x)$, con nivel de precisión alto.}
\label{fig:xnvsn-muestra-9}
\end{figure}

En el caso del método de Newton, sin embargo, los gráficos muestran diferencias notables entre distintas muestras y puntos iniciales; en la figura \ref{fig:xnvsn-casos-varios} se destacan casos patrones.

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.5]{graficos/XnVsN/Muestra-7-Newton-funcion-F-prec--50-tol--10e11.pdf}
  \includegraphics[scale=0.5]{graficos/XnVsN/Muestra-2-Newton-funcion-F-prec--50-tol--10e11.pdf}
  \includegraphics[scale=0.5]{graficos/XnVsN/Muestra-9-Newton-funcion-L-prec--50-tol--10e11.pdf} 
  \includegraphics[scale=0.5]{graficos/XnVsN/Muestra-8-Newton-funcion-F-prec--50-tol--10e11.pdf} 
\end{center}
\caption{Casos varios. Método de Newton. $x_n$ en función de $n$ para distintos $x_0$.}
\label{fig:xnvsn-casos-varios}
\end{figure}

\subsection{Eficiencia y error en el resultado}
\label{ref:resultados-eficiencia-error}

Para cada muestra y combinación de método y función objetivo, se midió el tiempo de ejecución, la cantidad de iteraciones, y el error relativo en el resultado, definido por: $|x-\tilde\beta| / |\tilde\beta|$, donde $x$ es el último punto de la sucesión y como $\tilde\beta$ se utilizó una aproximación obtenida con la menor tolerancia y mayor precisión posible.

El punto inicial para cada método se tomó con el criterio explicado en la sección \ref{sec:desarrollo-aplicacion-metodos-problema}.

En las figuras (\ref{fig:barras-eficiencia}) y (\ref{fig:barras-error}) se muestran gráficos comparando los resultados para cada combinación de método y función, en cada muestra.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{graficos/tiempoEjecucion/Tiempo-de-ejecucion-en-cada-muestra-[prec50-tol10e11].pdf}
\includegraphics[scale=0.6]{graficos/cantIteraciones/Cantidad-de-iteraciones-en-cada-muestra-[prec50-tol10e11].pdf}
\end{center}
\caption{Tiempo de ejecución y cantidad de iteraciones en cada muestra para todas las combinaciones de método y función objetivo.}
\label{fig:barras-eficiencia}
\end{figure}

\begin{figure}[h]
\begin{center}$
  \begin{array}{cc}
  \includegraphics[scale=0.6]{graficos/error/Error-relativo-en-cada-muestra-[prec50-tol10e09].pdf} \\
  \label{fig:XnvsN}
  \end{array}$
\end{center}
\caption{Error relativo respecto al resultado en cada muestra para todas las combinaciones de método y función objetivo.}
\label{fig:barras-error}
\end{figure}
 
\subsection{Ajuste a los datos}

Para evaluar como se ajusta la D$\Gamma$G ya parametrizada a las muestras de las cuales se estimaron los parámetros, se incluyen gráficos superponiendo el histograma de los datos y la función distribución de probabilidad de la D$\Gamma$G obtenida. Los resultados son similares para todas las muestras; se pueden observar en la figura (\ref{fig:ajuste}) ejemplos de conjuntos de datos de distintos tamaños y con formas variadas en su histograma.

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.5]{graficos/ajuste/Muestra-1-Biseccion-L-sig--243-beta--382-lamb--216.pdf}
  \includegraphics[scale=0.5]{graficos/ajuste/Muestra-3-Biseccion-L-sig--349-beta--138-lamb--117.pdf}
\end{center}
\begin{center}
  \includegraphics[scale=0.5]{graficos/ajuste/Muestra-8-Biseccion-L-sig--146-beta--821-lamb--338.pdf}
  \includegraphics[scale=0.5]{graficos/ajuste/Muestra-9-Biseccion-L-sig--204-beta--648-lamb--331.pdf}
\end{center}
\caption{Ajuste de la D$\Gamma$G al histograma de los datos. }
\label{fig:ajuste}
\end{figure}
 
\clearpage
 
\subsection{Experimentos sobre la precisión en el tipo de datos y tolerancia}

Se realizaron experimentos sobre dos niveles de precisión menores, en una primera instancia manteniendo la tolerancia utilizada previamente (figura (\ref{fig:xnvsn-muestra1-bajaprecision})), y luego ajustando el criterio de parada para admitir una condición de tolerancia más laxa (figura (\ref{fig:xnvsn-muestra1-bajaprecision-altatolerancia})).

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-1-Biseccion-funcion-F-prec--30-tol--10e11.pdf}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-1-Newton-funcion-F-prec--30-tol--10e11.pdf}
\end{center}
\caption{Muestra 1. $x_n$ en función de $n$ para distintos $x_0$ para ambos métodos, usando baja precisión y baja tolerancia.}
\label{fig:xnvsn-muestra1-bajaprecision}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-3-Biseccion-funcion-L-prec--30-tol--10e07.pdf}
  \includegraphics[scale=0.6]{graficos/XnVsN/Muestra-7-Newton-funcion-F-prec--40-tol--10e09.pdf}
\end{center}
\caption{Muestra 1. Método de bisección. $x_n$ en función de $n$ para distintos $x_0$, usando precisiones bajas y alta tolerancia.}
\label{fig:xnvsn-muestra1-bajaprecision-altatolerancia}
\end{figure}

Se incluye a continuación un gráfico mostrando el error en los resultados obtenidos al trabajar con menor precisión.

\begin{figure}[h]
\begin{center}
  \includegraphics[scale=0.6]{graficos/error/Error-relativo-en-cada-muestra-[prec40-tol10e09].pdf}
\end{center}
\caption{Error relativo para cada combinación de método y función objetivo al utilizar un nivel de precisión más limitado.}
\label{fig:error-bajaprecision}
\end{figure}

\clearpage