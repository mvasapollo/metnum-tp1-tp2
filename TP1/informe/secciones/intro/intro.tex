\subsection{Distribución gamma generalizada}
\label{sec:intro-dist-gamma-gen}
La distribución gamma generalizada (D$\Gamma$G) es una distribución de probabilidad continua, caracterizada por tres parámetros: $(\sigma, \beta, \lambda)$. Su función de densidad de probabilidad es la siguiente:

$$f_\Theta(x)=\frac{\beta\, x^{\beta \lambda - 1}}{\sigma^{\beta \lambda}\, \Gamma(\lambda)} \exp\left\{-\left(\frac{x}{\sigma} \right)^\beta \right\}\qquad\textrm{con } x\in\real_{>0}$$ donde 

$\Gamma(\cdot)$ es la funci\'on Gamma\footnote{La funci\'on Gamma, en el caso entero, es equivalente a la funci\'on factorial: $\Gamma(n)=n!$, $\forall n\in\nat$ } definida como $\Gamma(z)=\int_0^{\infty}{t^{z-1}e^{-t}\,dt}$, y $\Theta$ representa a la tupla de par\'ametros $\Theta=(\sigma,\beta,\lambda)$. El primero se encuentra relacionado con la escala de la funci\'on $f$ y los otros dos con la forma; todos son positivos.

Esta distribución es de interés ya que existen fenómenos naturales donde se presentan muestras distribuidas de esta forma, siendo utilizada en numerosas aplicaciones.
Además, otras distribuciones conocidas como la distribución Weibull o Gamma son casos particulares de la D$\Gamma$G ($\lambda = 1$ o $\beta = 1$, respectivamente), por lo que el desarrollo a continuación también puede ser utilizado en esos casos.

Un conjunto de datos con distribución D$\Gamma$G puede ser representado aproximadamente por los tres parámetros de su distribución, lo cual puede resultar conveniente cuando se requiere almacenar la información. Para calcularlos se utilizó una serie de estimadores, que son funciones en base a los datos para determinar los parámetros de la distribución que mejor se ajusta a ellos.

Llamando al conjunto de datos $X = \{x_1, x_2, ..., x_n\}$, los estimadores utilizados para los parámetros $\sigma$ y $\lambda$ se escriben de la siguiente forma:

\begin{eqnarray}
\tilde{\sigma} & = & \left(\frac{\sum_{i=1}^n{x_i^{\tilde{\beta}}}}{n \tilde{\lambda}} \right)^{1/\tilde{\beta}} \label{eqn:sigma}\\
\tilde{\lambda} & = &\left[ \tilde{\beta} \left( \frac{\sum_{i=1}^n{x_i^{\tilde{\beta}} \log x_i}}{\sum_{i=1}^n{x_i^{\tilde{\beta}}}} - \frac{\sum_{i=1}^n{\log x_i}}{n} \right)\right]^{-1} \label{eqn:lambda}
\end{eqnarray}

Por lo tanto, para obtener los valores de $\tilde\sigma$ y $\tilde\lambda$ es suficiente conocer el valor de $\beta$. Este puede ser aproximado a partir del estimador $\tilde\beta$, el cual se sabe verifica las siguientes ecuaciones:

\begin{eqnarray}
\log(\eme(2\beta)) -2\log(\eme(\beta)) & = & \log(1+\beta(\ere(\beta)-\ere(0))) \label{eqn:sol1}\\
\frac{\eme(2\beta)}{\eme^2(\beta)} & = & 1+\beta(\ere(\beta)-\ere(0))) \label{eqn:sol2}
\end{eqnarray}
donde 
\begin{eqnarray}
\eme(s) =  \frac{1}{n}\sum_{i=1}^n{x_i^s}\,, \qquad \emeh(s) = \frac{1}{n}\sum_{i=1}^n{x_i^s \log(x_i)}\,,  \qquad \ere(s) = \frac{\emeh(s)}{\eme(s)} \label{eqn:auxiliaries}
\end{eqnarray}

Se definen las siguientes funciones:

\begin{eqnarray}
L_X(x) & = & \log(\eme(2x)) -2\log(\eme(x)) - \log(1+x(\ere(x)-\ere(0))) \label{eqn:flogbeta}
\\
F_X(x) & = & \frac{\eme(2x)}{\eme^2(x)} - 1 - x(\ere(x)-\ere(0))) \label{eqn:fbeta}
\end{eqnarray}

Las ecuaciones $L_X(x) = 0$ y $F_X(x) = 0$ son equivalentes a (\ref{eqn:sol1}) y (\ref{eqn:sol2}) respectivamente. De esta forma, encontrar el valor de $\tilde\beta$ es equivalente a hallar un cero en la función $L_X(x)$ o $F_X(x)$, con $x>0$.

Se introducen a continuación los métodos utilizados para realizar esta operación.

\subsection{Método de bisección}
\label{sec:intro-biseccion}
Por el teorema de Bolzano, si $f:[a,b]\rightarrow\mathbb{R}$ es una función continua y $f(a) f(b) < 0$, entonces existe al menos un punto $c\in (a,b)$ donde $f(c) = 0$.\footnote{Ver referencia [1].}

Este teorema asegura que si una función es continua en un intervalo y toma valores con signos opuestos en sus bordes, entonces sin dudas habrá un cero en ese intervalo. El método de bisección, fundamentado por el teorema anterior, consiste en el siguiente procedimiento:

Dado el intervalo $[a,b]$ donde la función verifica las condiciones del teorema, y un natural $N$ que limita el máximo número de repeticiones que se realizarán:

\begin{center}
Calcular $p = \frac{a + b}{2}$ \\
Si $f(p) = 0$, o ya se han realizado $N$ repeticiones, terminar. \\
Si $f(a) f(p) < 0$, repetir el procedimiento en el subintervalo $[a,p]$. \\
De lo contrario, repetir el procedimiento en el subintervalo $[p,b]$. \\
\end{center}

Los sucesivos intervalos sobre los que se aplica el procedimiento continúan cumpliendo las condiciones del teorema\footnote{La función es continua en cualquier subintervalo de $[a,b]$, y por construcción siempre se toman intervalos en cuyos bordes la función tiene distinto signo.}, y además, la longitud de cada uno es la mitad de la longitud del intervalo previo. De esta forma, después de $k$ repeticiones, se obtiene un intervalo de longitud $\frac{b-a}{2^k}$ donde está garantizada la existencia de un cero. Tomando el centro del último intervalo como aproximación del cero, el error cometido es necesariamente menor a la mitad de la longitud del intervalo, $\frac{b-a}{2^{k+1}}$. Como esa expresión tiende a $0$ cuando $k\rightarrow \inf$, el método converge a un cero de la función.

Como primera observación, la existencia de un criterio de parada forzosa es necesaria incluso desde un punto de vista teórico. Si los puntos inciales a y b son racionales, entonces la sucesión de números donde se evalúa la función será racional, y por lo tanto jamás se llegará de forma exacta a un cero irracional; esto implicaría la no terminación del procedimiento en ciertos casos particulares. En este caso se eligió arbitrariamente una cota en el número de repeticiones. 

En segundo lugar, notar que el método converge a \emph{algún} cero de la función; si el intervalo inicial contiene más de uno, es difícil prever a cuál convergerá, lo cual puede ser un problema si se busca alguno en particular.

\subsection{Método de Newton}
Dada $f \in \mathcal{C}^2[a,b]$, se define la sucesión $\{x_i\}_{i=0}^{n+1}$ como: \\
$$x_0 \in [a,b]$$
\begin{equation}
x_{n+1} = g(x_n) = x_n + \frac{f(x_n)}{f'(x_n)}
\label{eqn:newton-g}
\end{equation}

Si hay un $p \in [a,b]$ con $f(p) = 0$ y $f'(p) \neq 0$, entonces existe un $\delta > 0$ tal que si $x_0 \in [p - \delta,p + \delta]$ , está garantizado que la sucesión converge a $p$ cuando $n \rightarrow \inf$.\footnote{Ver referencia [2].} 

El método de Newton consiste en tomar una primera aproximación $x_0$ tan cercana como sea posible al cero que se quiere calcular, e iterar la sucesión presentada previamente con ese valor inicial. El teorema nos asegura que existe un intervalo alrededor del cero donde el método convergerá satisfactoriamente. 

Naturalmente, si no se tiene información a priori sobre el cero que se pretende hallar, no será posible determinar un buen punto inicial; tampoco es sencillo estimar el ancho del intervalo donde está asegurada la convergencia, ya que depende fuertemente de la naturaleza de la función. Por estas razones, la convergencia global del método no está garantizada.

Para lidiar con estos problemas en la práctica, es posible utilizar métodos alternativos para obtener primeras aproximaciones al cero tan cercanas como se desee\footnote{Dentro de la aritmética finita.} como, por ejemplo, métodos gráficos o el método de bisección.