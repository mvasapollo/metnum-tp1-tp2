\subsection{Experimentación}

Se realizaron multiples experimentos tomando distintas configuraciones para los parámetros involucrados en la ejecución de los métodos, de forma tal de exponer las bondades de cada método para esta aplicación particular, y las ventajas de usar una u otra de las ecuaciones para obtener el estimador de $\beta$. 

Sin perder de vista el objetivo de estimar los parámetros sin intervención, se estudiaron las posibilidades de automatización que se presentan para cada combinación de método (bisección o Newton) y función objetivo ($L_X(x)$ o $F_X(x)$). Se buscaron condiciones para lograr la convergencia y luego se analizó, para cada combinación, la velocidad de convergencia en términos de cantidad de iteraciones y tiempo de ejecución, y la precisión del resultado obtenido. 

En todos los casos, se trabajó principalmente con alta precisión en el tipo de datos utilizado, y luego se analizó el impacto de alterar el criterio de parada de los métodos o la precisión en el cómputo de las funciones sobre los resultados obtenidos.

Finalmente, se utilizaron el método y la función más convenientes en términos de garantía de convergencia y precisión en el resultado para calcular de forma no supervisada los estimadores $\tilde\sigma$, $\tilde\beta$ y $\tilde\lambda$, analizando la calidad de ajuste de la distribución gamma generalizada ya parametrizada sobre el conjunto de datos.

\subsection{Aplicación de los métodos al problema}
\label{sec:desarrollo-aplicacion-metodos-problema}

Para estudiar el problema, se contó con 9 muestras generadas aleatoriamente con distribución gamma generalizada, cada una de ellas con un tamaño de entre 1200 y 10000 ejemplares. Como primera medida, se estudiaron las funciones $L_X(x)$ y $F_X(x)$ (presentadas en la sección \ref{sec:intro-dist-gamma-gen}) para cada uno de estos conjuntos de datos, buscando características comunes que permitan adecuar la utilización de los métodos descriptos a esta aplicación particular. Se realizaron gráficos para observar la forma de ambas funciones para cada muestra, así como la de sus derivadas. A continuación se muestran algunos casos representativos de los distintos patrones identificados:

\begin{figure}[h!]
\centering
	\includegraphics[scale=0.6]{graficos/funcionesLyF/muestra8funcionLhasta11.pdf}
	\includegraphics[scale=0.6]{graficos/funcionesLyF/muestra8funcionFhasta11.pdf}
	\caption{Gráfica de $L_X(x)$ y $F_X(x)$ para la muestra 8.}
	\label{fig:grafico-LyF-muestra8}
\end{figure}

Si bien en la figura (\ref{fig:grafico-LyF-muestra8}) solo se muestran los gráficos para el octavo conjunto de datos, las siguientes observaciones son comunes a todas las demás muestras. Ambas funciones tienen un comportamiento similar en el intervalo $[0, \tilde\beta]$. Las funciones toman el valor $0$ para $x = 0$, lo cual es sencillo verificar evaluando las ecuaciones (\ref{eqn:flogbeta}) y (\ref{eqn:fbeta}). Ambas funciones se vuelven negativas para $x>0$, alcanzan un mínimo y luego crecen rápidamente, intersectando el eje de las abcisas; además, todos los gráficos realizados parecen indicar que son estrictamente crecientes para $x > \tilde\beta$, lo que significaría que $\tilde\beta$ es el único cero para los reales positivos. En particular, se puede observar en los gráficos que el valor de $\tilde\beta$ para todas las muestras no supera a $10$;

Si se pudiera asegurar que todos los posibles conjuntos de datos comparten este comportamiento, esta información ya permitiría obtener de forma automática un intervalo donde aplicar el método de bisección con convergencia garantizada. Sería suficiente tomar un intervalo $[a, b]$ con $a > 0$ y muy pequeño, y $b$ cualquier valor donde la función toma un valor positivo; garantizando que el cero quede contenido en él. En la implementación esto se realizó buscando el primer $x > 0$ donde la función toma un valor positivo, como se puede ver en la sección \ref{subsec:MetnumBisectionMethod} del apéndice B.

\begin{figure}[h!]
\centering
	\includegraphics[scale=0.6]{graficos/funcionesLyF/muestra3funcionLhasta11.pdf}
	\includegraphics[scale=0.6]{graficos/funcionesLyF/muestra3funcionFhasta11.pdf}
	\caption{Gráfica de $L_X(x)$ y $F_X(x)$ para la muestra 3.}
	\label{fig:grafico-LyF-muestra3}
\end{figure}
Por otro lado, en la figura (\ref{fig:grafico-LyF-muestra3}) se observa la forma de las funciones para valores de $x$ alejados de $\tilde\beta$ en el caso de la tercera muestra. Se comparó con los gráficos generados para las demás muestras y todos mostraron características similares. Se puede inferir a partir de ellos que siempre hay un entorno alrededor del cero donde la derivada es estrictamente creciente y por lo tanto la función es convexa. Esto permite asegurar la convergencia del método de Newton si se toma un $x_0$ dentro de ese entorno\footnote{Ver referencia [3].}. En el caso de $F_X(x)$, en particular, todos los gráficos realizados parecen indicar que la función es convexa para todo $x > \tilde\beta$, permitiendo aún más versatilidad en la elección de la semilla. En la implementación se utilizó esta suposición, tomando un $x_0$ con el mismo criterio utilizado para elegir a $b$ en el método de bisección, como se puede verificar en la sección \ref{subsec:MetnumNewtonMethod} del apéndice B.

\subsection{Cómputo de las funciones}
\label{sec:computo-funciones}

Las funciones $L_X(x)$ y $F_X(x)$ se computaron según las expresiones (\ref{eqn:flogbeta}) y (\ref{eqn:fbeta}) de la sección \ref{sec:intro-dist-gamma-gen}, respectivamente.

Para el método de Newton únicamente se computó la función $g(x_n)$, de la ecuación (\ref{eqn:newton-g}), para lo cual fue necesario evaluar $L'_X(x)$ y $F'_X(x)$. A continuación se muestran las expresiones utilizadas para este fin, en términos de las funciones auxiliares definidas en (\ref{eqn:auxiliaries}):

\begin{equation}
L'_X(x) =  \frac{2 \emeh(2 x)}{\eme(2 x)} - \frac{2 \emeh(x)}{\eme(x)} + \frac{\ere(x) - \ere(0) + x \ereh(x)}{x (\ere(x) - \ere(0)) - 1 }	
\end{equation}

\begin{equation}
F'_X(x) = \frac{2 \emeh(2 x)}{\eme^2(x)} - \frac{2 \eme(2 x) \emeh(x)}{\eme^3(x)} - (\ere(x) - \ere(0) + x \ereh(x))
\end{equation}

Todas las funciones auxiliares contenidas en las expresiones consisten en sumatorias sobre los elementos del conjunto de datos, por lo cual se computaron todas en simultáneo realizando una única iteración sobre el conjunto. En la implementación también se precomputa el logaritmo de cada dato y el valor de $\ere(0)$, independientes del punto donde se evalúa la función. En la sección \ref{subsec:MetnumFunctor} del apéndice B se incluye el código correspondiente a lo discutido en este apartado.
   
\subsection{Criterio de parada}

Los gráficos de la figura (\ref{fig:grafico-LyF-muestra8}) muestran que las funciones objetivo cruzan el eje de las abcisas en un punto distinguido, al contrario de un acercamiento asintótico que dificulte distinguir el punto exacto de intersección. Esta observación sugiere que el siguiente criterio de parada puede ser adecuado: \\
Dado un $\epsilon > 0$, al que llamaremos tolerancia, considerar que el método ha convergido a $x_n = \tilde\beta$ una vez que $|L_X(x_n)| < \epsilon$ o $|F_X(x_n)| < \epsilon$ respectivamente. 

Se consideraron, además, otras dos alternativas: 

\begin{itemize}
\item $|x_n - \tilde\beta| < \epsilon$
\item $\frac{|x_n - x_{n-1}|}{|x_n|} < \epsilon$
\end{itemize}

La primera de ellas se descartó dado que $\tilde\beta$ es desconocido a priori. La segunda alternativa resultó viable, pero mostró ser inadecuada para la comparación entre los dos métodos. Por lo tanto, en la implementación se utilizó el criterio descripto previamente, permitiendo variar el valor de la tolerancia como un parámetro de la experimentación, como se puede verificar en las secciones \ref{subsec:MetnumBisectionMethod} y \ref{subsec:MetnumNewtonMethod} del apéndice B.

\subsection{Precisión en el tipo de datos}

Los métodos fueron implementados en base a un tipo de datos de tipo punto flotante, cuyo nivel de precisión fue un parámetro de estudio. Partiendo de un punto flotante de doble precisión con una mantissa de 53 bits, se realizaron truncamientos para obtener menor precision; a saber, mantissas de 30, 40 y 50 bits. El truncamiento se realizó anulando la cantidad suficiente de bits menos significativos, mediante un enmascaramiento bit a bit de la mantissa. Esto permitió realizar el recorte del tipo de datos en una cantidad de tiempo independiente de la precisión buscada, permitiendo comparar entre tiempos de ejecución para corridas con distintas precisiones. En la sección \ref{subsec:TFloat} del apéndice B se incluye la implementación.