files = [1:9];

plotX = files;
plotY = [];

precision = 30;
tolerance = 1e-5;

for file=files

	eval(['X = X' num2str(file) ';']);

	sequence = getView(X, {{'method',@(x)strcmp(x,'bisection')},				...
								{'precision',@(x)x==50},						...
								{'x0',@(x)x == 10},								...
								{'tolerance',@(x)x==1e-11},						...
								{'useLogFunction',@(x)x==1},					...
							}, {'sequence.value'});
	root = sequence{1}(end);

	resultBisecL = getView(X, {{'method',@(x)strcmp(x,'bisection')},		...
								{'precision',@(x)x==precision},						...
								{'x0',@(x)x > root},							...
								{'tolerance',@(x)x==tolerance},						...
								{'useLogFunction',@(x)x==1},					...
							}, {'numIterations','totalTime'});
	resultBisecL = resultBisecL(1,:);
	numIterationsBisecL = resultBisecL{1};
	if( numIterationsBisecL ~= 101 )
		totalTimeBisecL = resultBisecL{2};
	else
		totalTimeBisecL = 0;
	end
	resultBisecF = getView(X, {{'method',@(x)strcmp(x,'bisection')},		...
									{'precision',@(x)x==precision},					...
									{'x0',@(x)x > root},						...
									{'tolerance',@(x)x==tolerance},					...
									{'useLogFunction',@(x)x==0},				...
								}, {'numIterations','totalTime'});
	resultBisecF = resultBisecF(1,:);
	numIterationsBisecF = resultBisecF{1};
	if( numIterationsBisecF ~= 101 )
		totalTimeBisecF = resultBisecF{2};
	else
		totalTimeBisecF = 0;
	end
	resultNewtonL = getView(X, {{'method',@(x)strcmp(x,'newton')},		...
									{'precision',@(x)x==precision},					...
									{'x0',@(x)x > root},						...
									{'tolerance',@(x)x==tolerance},					...
									{'useLogFunction',@(x)x==1},				...
								}, {'numIterations','totalTime'});
	resultNewtonL = resultNewtonL(1,:);
	numIterationsNewtonL = resultNewtonL{1};
	if( numIterationsNewtonL ~= 101 )
		totalTimeNewtonL = resultNewtonL{2};
	else
		totalTimeNewtonL = 0;
	end
	resultNewtonF = getView(X, {{'method',@(x)strcmp(x,'newton')},		...
									{'precision',@(x)x==precision},					...
									{'x0',@(x)x > root},						...
									{'tolerance',@(x)x==tolerance},					...
									{'useLogFunction',@(x)x==0},				...
								}, {'numIterations','totalTime'});
	resultNewtonF = resultNewtonF(1,:);
	numIterationsNewtonF = resultNewtonF{1};
	if( numIterationsNewtonF ~= 101 )
		totalTimeNewtonF = resultNewtonF{2};
	else
		totalTimeNewtonF = 0;
	end
	plotY = [plotY; totalTimeBisecL, totalTimeBisecF, totalTimeNewtonL, totalTimeNewtonF];
end

figure
bar(plotX, plotY, 'grouped');
ylim([0  (max(max(plotY))*1.05)]);
description = ['Tiempo de ejecucion en cada muestra [prec=' num2str(precision) ', tol=' num2str(tolerance, '%10.1e\n') ']'];
title(description);
xlabel('Muestra');
ylabel('Tiempo (microseg)');
legend('Biseccion (L)', 'Biseccion (F)', 'Newton (L)', 'Newton (F)');

description = strrep(description, '.', '');
description = strrep(description, '-', '');
description = strrep(description, '=', '');
description = strrep(description, ',', '');
description = strrep(description, ' ', '-');
export_fig(['tiempoEjecucion/' description '.pdf']);