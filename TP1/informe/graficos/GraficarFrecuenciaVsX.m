files = [1:9];
for file=files
	[data n] = leer_datos(['../../datos/datos_todos/X' num2str(file) '.txt']);
	eval(['X = X' num2str(file) ';']);

	sigmaBetaLambda = getView(X, {{'method',@(x)strcmp(x,'bisection')},		...
								{'precision',@(x)x==50},					...
								{'x0',@(x)x == 10},							...
								{'tolerance',@(x)x==1e-11},					...
								{'useLogFunction',@(x)x==1},				...
							}, {'sigma', 'beta', 'lambda'});
	sigma = sigmaBetaLambda{1};
	beta = sigmaBetaLambda{2};
	lambda = sigmaBetaLambda{3};
	dibujarHistyAjuste(data, sigma, beta, lambda, ['Muestra ' num2str(file) ', Biseccion, L, sig = ' num2str(sigma, 3) ', beta = ' num2str(beta, 3) ', lamb = ' num2str(lambda, 3)]);
end

close all