function biplotFxVsX(file, funName, interval, Fx, Fpx, plotIndex)

	cc=copper(2);
	figure;
	hold on;
	plot(interval(1:plotIndex), zeros(1,plotIndex), 'color', 'black');
	axis = [];
	legends = [];
	axis = [axis; plot(interval(1:plotIndex),Fx(1:plotIndex),'color',cc(1,:))];
	legends = [legends; [funName ' ']];
	axis = [axis; plot(interval(1:plotIndex),Fpx(1:plotIndex),'color',cc(2,:),'LineStyle','--')];
	legends = [legends; [funName '''']];
	legend(axis, legends);
	title(['Funcion ' funName '(x) para la muestra ' num2str(file)]);
	xlabel('x');
	ylabel([funName '(x) o ' funName '''(x)']);

	filename = ['funcionesLyF/muestra-' num2str(file) '-funcion-' funName '-hasta-' num2str(interval(plotIndex))];
	filename = strrep(filename, '.', '');
	filename = strrep(filename, '-', '');
	filename = strrep(filename, '=', '');
	filename = strrep(filename, ',', '');
	filename = strrep(filename, ' ', '-');

	export_fig([filename '.pdf']);
end