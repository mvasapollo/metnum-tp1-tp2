function multiplotXnVsN(sequences, description)
	figure;
	hold on;
	xlim([1 inf]);
	ylim([0 11]);
	title(['Xn vs n (' description ')']);
	ylabel('Xn');
	xlabel('n');
	cc=copper(length(sequences));
	for i=1:length(sequences)
		seq = sequences{i};
		if( length(seq) == 101 || sum(isnan(seq)) > 0 )
			plot([1],[seq(1)],'xr','MarkerSize',10,'LineWidth', 3);
		else
			plot(seq, 'k', 'color', cc(i,:));
		end
	end
	hold off;

	description = strrep(description, '.', '');
	description = strrep(description, '-', '');
	description = strrep(description, '=', '');
	description = strrep(description, ',', '');
	description = strrep(description, ' ', '-');
	export_fig(['XnVsN/' description '.pdf']);
end