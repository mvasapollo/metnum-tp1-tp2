close all

interval = 0:0.05:11;

files = [1:9];
for file=files
	[data n] = leer_datos(['../../datos/datos_todos/X' num2str(file) '.txt']);
	[Lx, Lpx, Fx, Fpx] = computarLyF(data, n, interval);
	rootIndex = find(Lx == min(abs(Lx(Lx>0))));

	biplotFxVsX(file, 'F', interval, Fx, Fpx, length(interval));

	biplotFxVsX(file, 'L', interval, Lx, Lpx, length(interval));

	biplotFxVsX(file, 'F', interval, Fx, Fpx, rootIndex + 40);

	biplotFxVsX(file, 'L', interval, Lx, Lpx, rootIndex + 40);
end