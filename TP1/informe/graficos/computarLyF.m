function [Lx, Lpx, Fx, Fpx] = computarLyF(data, n, interval)

	function Mx = M(x)
		Mx = (1/n)*sum(data.^x);
	end

	function Mpx = Mp(x)
		Mpx = (1/n)*sum(data.^x .* log(data));
	end

	function Mppx = Mpp(x)
		Mppx = (1/n)*sum(data.^x .* log(data) .* log(data));
	end

	function Rx = R(x)
		Rx = Mp(x)/M(x);
	end

	function Fx = F(x)
		Fx = M(2*x)/(M(x))^2 - 1 - x * (R(x) - R(0));
	end

	function Fpx = Fp(x)
		Fpx = ((M(x))^3 * Mp(0) - 2 * M(2 * x) * Mp(x) + M(x) * (x * (Mp(x))^2 + 2 * Mp(2 * x)) - (M(x))^2 * (Mp(x) + x * Mpp(x)))/(M(x))^3;
	end
	
	function Lx = L(x)
		Lx = log(M(2*x)/(M(x))^2) - log(1 + x * (R(x) - R(0)));
	end

	function Lpx = Lp(x)
		Lpx = (x * M(2 * x) * (Mp(x))^2 - (M(x))^2 * (M(2 * x) * Mp(0) - 2 * (-1 + x * Mp(0)) * Mp(2 * x)) + M(x) * (-2 * x * Mp(x) * Mp(2 * x) + M(2 * x) * ((3 - 2 * x * Mp(0)) * Mp(x) + x * Mpp(x))))/(M(x) * M(2 * x) * (M(x) * (-1 + x * Mp(0)) - x * Mp(x)));
	end

	Lx = arrayfun(@L ,interval);
	Lpx = arrayfun(@Lp ,interval);
	Fx = arrayfun(@F ,interval);
	Fpx = arrayfun(@Fp ,interval);
end