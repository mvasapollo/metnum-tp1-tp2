function v = getView(X, conditions, columns)

	allColumns = {'method', 'x0', 'precision', 'tolerance', 'useLogFunction', 'sequence.value', 'numIterations', 'sequence.time', 'totalTime', 'beta', 'lambda', 'sigma', 'abs(fbeta)'};

	if nargin < 3
		columns = allColumns;
	end
	if ~iscell(columns)
		error('Columns must be a cell array.');
	end

	function colNum = colName2ColNum(colName)
		colNum = -1;
		for index = 1:length(allColumns)
			if(strcmp(allColumns{index},colName) == true)
				colNum = index;
				break;
			end
		end
		if(colNum == -1)
			error('Invalid column specified.');
		end
	end

	returnColumns = [];
	for col=columns
		returnColumns(end+1) = colName2ColNum(col{1});
	end
	
	indexes = [];
	for i=1:size(X,1)
		includeRow = true;
		for condition=conditions
			col = colName2ColNum(condition{1}{1});
			value = cell2mat(X(i,col));
			fun = condition{1}{2};
			if(fun(value) == false)
				includeRow = false;
				break;
			end
		end
		if includeRow
			indexes(end+1) = i;
		end
	end
	
	v = X(indexes,returnColumns);	
end
