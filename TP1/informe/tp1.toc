\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n te\IeC {\'o}rica}{3}
\contentsline {subsection}{\numberline {1.1}Distribuci\IeC {\'o}n gamma generalizada}{3}
\contentsline {subsection}{\numberline {1.2}M\IeC {\'e}todo de bisecci\IeC {\'o}n}{3}
\contentsline {subsection}{\numberline {1.3}M\IeC {\'e}todo de Newton}{4}
\contentsline {section}{\numberline {2}Desarrollo}{5}
\contentsline {subsection}{\numberline {2.1}Experimentaci\IeC {\'o}n}{5}
\contentsline {subsection}{\numberline {2.2}Aplicaci\IeC {\'o}n de los m\IeC {\'e}todos al problema}{5}
\contentsline {subsection}{\numberline {2.3}C\IeC {\'o}mputo de las funciones}{6}
\contentsline {subsection}{\numberline {2.4}Criterio de parada}{6}
\contentsline {subsection}{\numberline {2.5}Precisi\IeC {\'o}n en el tipo de datos}{7}
\contentsline {section}{\numberline {3}Resultados}{8}
\contentsline {subsection}{\numberline {3.1}Experimentos sobre $x_0$}{8}
\contentsline {subsection}{\numberline {3.2}Eficiencia y error en el resultado}{8}
\contentsline {subsection}{\numberline {3.3}Ajuste a los datos}{9}
\contentsline {subsection}{\numberline {3.4}Experimentos sobre la precisi\IeC {\'o}n en el tipo de datos y tolerancia}{12}
\contentsline {section}{\numberline {4}Discusi\IeC {\'o}n}{14}
\contentsline {subsection}{\numberline {4.1}Garant\IeC {\'\i }as de convergencia}{14}
\contentsline {subsection}{\numberline {4.2}Comparaciones de eficiencia}{14}
\contentsline {subsection}{\numberline {4.3}Error en el resultado y ajuste a los datos}{14}
\contentsline {subsection}{\numberline {4.4}Precisi\IeC {\'o}n}{14}
\contentsline {section}{\numberline {5}Conclusiones}{16}
\contentsline {section}{\numberline {6}Ap\IeC {\'e}ndices}{17}
\contentsline {subsection}{\numberline {6.1}Ap\IeC {\'e}ndice A: Enunciado del TP}{17}
\contentsline {subsection}{\numberline {6.2}Ap\IeC {\'e}ndice B: Codigo fuente}{17}
\contentsline {subsubsection}{\numberline {6.2.1}MetnumMethod.h}{17}
\contentsline {subsubsection}{\numberline {6.2.2}MetnumBisectionMethod.cpp}{18}
\contentsline {subsubsection}{\numberline {6.2.3}MetnumNewtonMethod.cpp}{19}
\contentsline {subsubsection}{\numberline {6.2.4}MetnumFunctor.cpp}{20}
\contentsline {subsubsection}{\numberline {6.2.5}TFloat.cpp}{22}
\contentsline {section}{\numberline {7}Referencias}{25}
