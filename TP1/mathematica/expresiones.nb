(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     12465,        400]
NotebookOptionsPosition[     11193,        351]
NotebookOutlinePosition[     11528,        366]
CellTagsIndexPosition[     11485,        363]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"ClearAll", "[", 
  RowBox[{"Evaluate", "[", 
   RowBox[{"$Context", "<>", "\"\<*\>\""}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.5745943929929132`*^9, 3.574594411327176*^9}, {
  3.574594474300446*^9, 3.574594488221538*^9}, {3.5745945736823587`*^9, 
  3.5745946049306297`*^9}}],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"Funciones", " ", "F", " ", "y", " ", "L"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.5745995646142607`*^9, 3.574599566805201*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"F", "[", "x_", "]"}], "=", 
  RowBox[{
   FractionBox[
    RowBox[{"M", "[", 
     RowBox[{"2", "x"}], "]"}], 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"M", "[", "x", "]"}], ")"}], "^", "2"}]], "-", "1", "-", 
   RowBox[{"x", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"R", "[", "x", "]"}], "-", 
      RowBox[{"R", "[", "0", "]"}]}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.574593901190255*^9, 3.574593936701866*^9}, {
  3.574594049014867*^9, 3.5745940522868423`*^9}, {3.574599529556518*^9, 
  3.574599529746621*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "1"}], "+", 
  FractionBox[
   RowBox[{"M", "[", 
    RowBox[{"2", " ", "x"}], "]"}], 
   SuperscriptBox[
    RowBox[{"M", "[", "x", "]"}], "2"]], "-", 
  RowBox[{"x", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      RowBox[{"R", "[", "0", "]"}]}], "+", 
     RowBox[{"R", "[", "x", "]"}]}], ")"}]}]}]], "Output",
 CellChangeTimes->{{3.574594385323319*^9, 3.574594413876254*^9}, {
   3.574594608148102*^9, 3.5745946212548037`*^9}, {3.5745946547601633`*^9, 
   3.5745946675091953`*^9}, 3.574594749556754*^9, 3.5745952950841*^9, 
   3.574597939559454*^9, 3.574597973653324*^9, 3.574598742549548*^9, 
   3.57459953632537*^9, {3.57459957052623*^9, 3.574599600126964*^9}, {
   3.57459973235217*^9, 3.574599743328518*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"L", "[", "x_", "]"}], "=", 
  RowBox[{
   RowBox[{"Log", "[", 
    FractionBox[
     RowBox[{"M", "[", 
      RowBox[{"2", "x"}], "]"}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"M", "[", "x", "]"}], ")"}], "^", "2"}]], "]"}], "-", 
   RowBox[{"Log", "[", 
    RowBox[{"1", "+", 
     RowBox[{"x", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"R", "[", "x", "]"}], "-", 
        RowBox[{"R", "[", "0", "]"}]}], ")"}]}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.574593977670362*^9, 3.57459400334301*^9}, {
   3.574594065123644*^9, 3.574594065517521*^9}, 3.5745995203682213`*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Log", "[", 
   FractionBox[
    RowBox[{"M", "[", 
     RowBox[{"2", " ", "x"}], "]"}], 
    SuperscriptBox[
     RowBox[{"M", "[", "x", "]"}], "2"]], "]"}], "-", 
  RowBox[{"Log", "[", 
   RowBox[{"1", "+", 
    RowBox[{"x", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"R", "[", "0", "]"}]}], "+", 
       RowBox[{"R", "[", "x", "]"}]}], ")"}]}]}], "]"}]}]], "Output",
 CellChangeTimes->{{3.574594385363215*^9, 3.574594405370296*^9}, 
   3.57459462126182*^9, {3.574594661350069*^9, 3.5745946675161467`*^9}, 
   3.5745947495713654`*^9, 3.5745952951048603`*^9, 3.5745979395873013`*^9, 
   3.574597973673687*^9, 3.574598742557736*^9, 3.574599536332299*^9, {
   3.5745995705546207`*^9, 3.574599600143292*^9}, {3.574599732381158*^9, 
   3.574599743351397*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"derivada", " ", "de", " ", "F", " ", "y", " ", "L"}], " ", 
  "*)"}]], "Input",
 CellChangeTimes->{{3.574599557212459*^9, 3.574599559954076*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"F", "[", "x", "]"}], ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.574593968201569*^9, 3.574593974728286*^9}, {
  3.574594100555324*^9, 3.5745941045826273`*^9}, {3.574596917425869*^9, 
  3.574596932770713*^9}, {3.5745979283355083`*^9, 3.574597930386697*^9}, {
  3.57459876440016*^9, 3.574598790038403*^9}, {3.574599531635257*^9, 
  3.57459953183718*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", "0", "]"}], "-", 
  RowBox[{"R", "[", "x", "]"}], "-", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{"M", "[", 
     RowBox[{"2", " ", "x"}], "]"}], " ", 
    RowBox[{
     SuperscriptBox["M", "\[Prime]",
      MultilineFunction->None], "[", "x", "]"}]}], 
   SuperscriptBox[
    RowBox[{"M", "[", "x", "]"}], "3"]], "+", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     SuperscriptBox["M", "\[Prime]",
      MultilineFunction->None], "[", 
     RowBox[{"2", " ", "x"}], "]"}]}], 
   SuperscriptBox[
    RowBox[{"M", "[", "x", "]"}], "2"]], "-", 
  RowBox[{"x", " ", 
   RowBox[{
    SuperscriptBox["R", "\[Prime]",
     MultilineFunction->None], "[", "x", "]"}]}]}]], "Output",
 CellChangeTimes->{{3.5745943853693953`*^9, 3.574594405400666*^9}, 
   3.574594621295252*^9, {3.57459466138286*^9, 3.574594667542282*^9}, {
   3.574594744626729*^9, 3.574594749601924*^9}, 3.5745952951472673`*^9, {
   3.5745969225469303`*^9, 3.574596933219492*^9}, {3.5745979308051558`*^9, 
   3.574597939615609*^9}, 3.574597973690942*^9, {3.574598742574568*^9, 
   3.5745987903488092`*^9}, 3.57459953633814*^9, {3.574599570590149*^9, 
   3.574599600163313*^9}, {3.5745997324104347`*^9, 3.574599743385648*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"L", "[", "x", "]"}], ",", "x"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.574594007464007*^9, 3.574594011926857*^9}, {
   3.574594107865333*^9, 3.5745941112283154`*^9}, {3.574594181079563*^9, 
   3.574594250143426*^9}, {3.574594625150568*^9, 3.574594741751685*^9}, {
   3.5745981607288713`*^9, 3.5745982024169083`*^9}, {3.57459823818778*^9, 
   3.5745982398406353`*^9}, {3.57459872115016*^9, 3.574598759377493*^9}, {
   3.574598802116228*^9, 3.574598860361299*^9}, {3.574599255993367*^9, 
   3.574599260518073*^9}, 3.574599522876918*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   FractionBox[
    RowBox[{"2", " ", 
     RowBox[{
      SuperscriptBox["M", "\[Prime]",
       MultilineFunction->None], "[", "x", "]"}]}], 
    RowBox[{"M", "[", "x", "]"}]]}], "+", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     SuperscriptBox["M", "\[Prime]",
      MultilineFunction->None], "[", 
     RowBox[{"2", " ", "x"}], "]"}]}], 
   RowBox[{"M", "[", 
    RowBox[{"2", " ", "x"}], "]"}]], "+", 
  FractionBox[
   RowBox[{
    RowBox[{"-", 
     RowBox[{"R", "[", "0", "]"}]}], "+", 
    RowBox[{"R", "[", "x", "]"}], "+", 
    RowBox[{"x", " ", 
     RowBox[{
      SuperscriptBox["R", "\[Prime]",
       MultilineFunction->None], "[", "x", "]"}]}]}], 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"x", " ", 
     RowBox[{"R", "[", "0", "]"}]}], "-", 
    RowBox[{"x", " ", 
     RowBox[{"R", "[", "x", "]"}]}]}]]}]], "Output",
 CellChangeTimes->{{3.574598179710278*^9, 3.574598203014645*^9}, 
   3.574598240191044*^9, {3.574598724479971*^9, 3.574598759852984*^9}, {
   3.5745988041303797`*^9, 3.574598813236841*^9}, {3.5745988447274446`*^9, 
   3.574598860700075*^9}, 3.574599260809083*^9, 3.574599536393214*^9, {
   3.5745995706643953`*^9, 3.5745996002370367`*^9}, {3.5745997324818697`*^9, 
   3.57459974345555*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"lambda", " ", "y", " ", "sigma"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.5745996776948347`*^9, 3.574599682156152*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"lambda", "[", "x_", "]"}], "=", 
  FractionBox["1", 
   RowBox[{"x", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"R", "[", "x", "]"}], "-", 
      RowBox[{"R", "[", "0", "]"}]}], ")"}]}]]}]], "Input",
 CellChangeTimes->{{3.574599685370537*^9, 3.5745996855422993`*^9}, 
   3.574599739348723*^9}],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{"x", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      RowBox[{"R", "[", "0", "]"}]}], "+", 
     RowBox[{"R", "[", "x", "]"}]}], ")"}]}]]], "Output",
 CellChangeTimes->{{3.574599732510765*^9, 3.574599743490549*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"sigma", "[", "x_", "]"}], "=", 
  RowBox[{
   RowBox[{"(", 
    FractionBox[
     RowBox[{"M", "[", "x", "]"}], 
     RowBox[{"lambda", "[", "x", "]"}]], ")"}], "^", 
   RowBox[{"(", 
    RowBox[{"1", "/", "x"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.5745996958965683`*^9, 3.574599729394917*^9}}],

Cell[BoxData[
 SuperscriptBox[
  RowBox[{"(", 
   RowBox[{"x", " ", 
    RowBox[{"M", "[", "x", "]"}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"R", "[", "0", "]"}]}], "+", 
      RowBox[{"R", "[", "x", "]"}]}], ")"}]}], ")"}], 
  FractionBox["1", "x"]]], "Output",
 CellChangeTimes->{{3.574599732539102*^9, 3.574599743515212*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{"R", "[", "x", "]"}], " ", "y", " ", 
   RowBox[{
    RowBox[{"R", "'"}], "[", "x", "]"}]}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.574599493560807*^9, 3.574599512556481*^9}, {
  3.574599583306294*^9, 3.574599586818465*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", "x_", "]"}], "=", 
  FractionBox[
   RowBox[{
    RowBox[{"M", "'"}], "[", "x", "]"}], 
   RowBox[{"M", "[", "x", "]"}]]}]], "Input",
 CellChangeTimes->{{3.574599589040889*^9, 3.574599594495412*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["M", "\[Prime]",
    MultilineFunction->None], "[", "x", "]"}], 
  RowBox[{"M", "[", "x", "]"}]]], "Output",
 CellChangeTimes->{
  3.574599600273158*^9, {3.574599732575768*^9, 3.574599743550466*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Together", "[", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"R", "[", "x", "]"}], ",", "x"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.574595432573742*^9, 3.574595442581841*^9}, {
  3.5745971484662247`*^9, 3.5745971518062487`*^9}, {3.5745995938840313`*^9, 
  3.574599597875347*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"-", 
    SuperscriptBox[
     RowBox[{
      SuperscriptBox["M", "\[Prime]",
       MultilineFunction->None], "[", "x", "]"}], "2"]}], "+", 
   RowBox[{
    RowBox[{"M", "[", "x", "]"}], " ", 
    RowBox[{
     SuperscriptBox["M", "\[Prime]\[Prime]",
      MultilineFunction->None], "[", "x", "]"}]}]}], 
  SuperscriptBox[
   RowBox[{"M", "[", "x", "]"}], "2"]]], "Output",
 CellChangeTimes->{
  3.574595442931076*^9, 3.574597152091597*^9, 3.574597939711985*^9, 
   3.57459797378581*^9, 3.574598742648548*^9, 3.5745995364230328`*^9, {
   3.574599570700528*^9, 3.574599600301255*^9}, {3.574599732600293*^9, 
   3.574599743575716*^9}}]
}, Open  ]]
},
WindowSize->{1366, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 306, 6, 30, "Input"],
Cell[866, 28, 180, 3, 30, "Input"],
Cell[CellGroupData[{
Cell[1071, 35, 567, 17, 57, "Input"],
Cell[1641, 54, 767, 19, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2445, 78, 633, 19, 57, "Input"],
Cell[3081, 99, 824, 21, 50, "Output"]
}, Open  ]],
Cell[3920, 123, 191, 4, 30, "Input"],
Cell[CellGroupData[{
Cell[4136, 131, 416, 8, 30, "Input"],
Cell[4555, 141, 1243, 31, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5835, 177, 629, 11, 30, "Input"],
Cell[6467, 190, 1289, 37, 51, "Output"]
}, Open  ]],
Cell[7771, 230, 171, 3, 30, "Input"],
Cell[CellGroupData[{
Cell[7967, 237, 332, 10, 57, "Input"],
Cell[8302, 249, 270, 8, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8609, 262, 336, 10, 56, "Input"],
Cell[8948, 274, 363, 11, 38, "Output"]
}, Open  ]],
Cell[9326, 288, 287, 7, 30, "Input"],
Cell[CellGroupData[{
Cell[9638, 299, 243, 7, 57, "Input"],
Cell[9884, 308, 256, 7, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10177, 320, 313, 7, 30, "Input"],
Cell[10493, 329, 684, 19, 53, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

