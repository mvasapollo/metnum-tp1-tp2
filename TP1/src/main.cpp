#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;

#include "TFloat/TFloat.h"
#include "MetnumMethods/MetnumMethod.h"
#include "MetnumMethods/MetnumNewtonMethod.h"
#include "MetnumMethods/MetnumBisectionMethod.h"
#include "MetnumFileLoader.h"
#include "MetnumCmdArgs.h"

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(*arr))

void exportToMatlabCellArray( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args );
void perfTable( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args );
void computeParameters( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args );

void getEvaluationValues( MetnumCmdArgs& args, vector<double>& x0Values, vector<size_t>& precisionValues, vector<double>& toleranceValues, vector<int>& useLogFunctionValues );
void printMatlabCellArrayRow( const char* method, MetnumMethodInput& input, MetnumMethodOutput& output );
double stringToDouble( string& str );

int main( int argc, char** argv ) {

    try {
        MetnumCmdArgs args;

        if( !parseMetnumCmdArgs( argc, argv, args ) ) {
            printHelp( string( argv[0] ) );
            return 0;
        }

        vector<string>::const_iterator it;

        for( it = args.fileNames.begin(); it != args.fileNames.end(); ++it ) {

            vector<double> values = loadFile( *it );
            MetnumMethodInput input( values );

            if( args.exp != CMD_ARG_NOT_SET ) {
                exportToMatlabCellArray( *it, input, args );
            }

            if( args.perf != CMD_ARG_NOT_SET ) {
                perfTable( *it, input, args );
            }

            if( args.exp == CMD_ARG_NOT_SET && args.perf == CMD_ARG_NOT_SET ) {
                computeParameters( *it, input, args );
            }
        }

    } catch( string& msg ) {
        cerr << "Error | " << msg << endl;
        return -1;
    }

    return 0;
}

inline string getNameFromFileName( const string& fileName ) {
    size_t beginName = fileName.find_last_of( '/' ) + 1, endName = fileName.find_last_of( '.' ) - 1;
    return fileName.substr( beginName, endName - beginName + 1 );
}

void exportToMatlabCellArray( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args ) {

    cout << "\% method x0 precision tolerance useLogFunction sequence.value sequence.time totalTime beta lambda sigma abs(fbeta)" << endl << endl;

    cout << getNameFromFileName( fileName ) << " = {";

    cout.precision( 10 );

    vector<double> x0Values;
    vector<size_t> precisionValues;
    vector<double> toleranceValues;
    vector<int> useLogFunctionValues;
    getEvaluationValues( args, x0Values, precisionValues, toleranceValues, useLogFunctionValues );

    vector<double>::iterator x0It;
    vector<size_t>::iterator precisionIt;
    vector<double>::iterator toleranceIt;
    vector<int>::iterator useLogFunctionIt;

    for( x0It = x0Values.begin(); x0It != x0Values.end(); ++x0It ) {
        for( precisionIt = precisionValues.begin(); precisionIt != precisionValues.end(); ++precisionIt ) {
            for( toleranceIt = toleranceValues.begin(); toleranceIt != toleranceValues.end(); ++toleranceIt ) {
                for( useLogFunctionIt = useLogFunctionValues.begin(); useLogFunctionIt != useLogFunctionValues.end(); ++useLogFunctionIt ) {

                    input.x0 = TFloat( *x0It, input.precision );
                    input.precision = *precisionIt;
                    input.tolerance = *toleranceIt;
                    input.useLogFunction = *useLogFunctionIt;
                    MetnumMethodOutput output = runMetnumBisectionMethod( input );
                    printMatlabCellArrayRow( "bisection", input, output );
                    output = runMetnumNewtonMethod( input );
                    printMatlabCellArrayRow( "newton", input, output );
                }
            }
        }
    }

    cout << "};" << endl;
}

void getEvaluationValues( MetnumCmdArgs& args, vector<double>& x0Values, vector<size_t>& precisionValues, vector<double>& toleranceValues, vector<int>& useLogFunctionValues ) {
    if( args.x0 != CMD_ARG_NOT_SET ) {
        x0Values.push_back( args.x0 );

    } else {
        double x0ValuesArr[] = {3, 4, 5, 6, 7, 8, 9, 10};
        x0Values = vector<double>( x0ValuesArr, x0ValuesArr + ARRAY_SIZE( x0ValuesArr ) );
    }

    if( args.prec != CMD_ARG_NOT_SET ) {
        precisionValues.push_back( args.prec );

    } else {
        size_t precisionValuesArr[] = {20, 30, 40, 50};
        precisionValues = vector<size_t>( precisionValuesArr, precisionValuesArr + ARRAY_SIZE( precisionValuesArr ) );
    }

    if( args.tol != CMD_ARG_NOT_SET ) {
        toleranceValues.push_back( pow( 10, -args.tol ) );

    } else {
        double toleranceValuesArr[] = {1e-5, 1e-7, 1e-9, 1e-11};
        toleranceValues = vector<double>( toleranceValuesArr, toleranceValuesArr + ARRAY_SIZE( toleranceValuesArr ) );
    }

    if( args.logf != CMD_ARG_NOT_SET ) {
        useLogFunctionValues.push_back( args.logf );

    } else {
        useLogFunctionValues.push_back( 0 ); useLogFunctionValues.push_back( 1 );
    }
}

// method x0 precision tolerance useLogFunction sequence.value numIterations sequence.time totalTime beta lambda sigma abs(fbeta)
void printMatlabCellArrayRow( const char* method, MetnumMethodInput& input, MetnumMethodOutput& output ) {
    cout << "\'" << method << "\'" << ",";
    cout << input.x0.dbl() << ",";
    cout << input.precision << ",";
    cout << input.tolerance << ",";
    cout << input.useLogFunction << ",";
    vector<TupleTimeValue>::const_iterator it;
    cout << "[";

    for( it = output.sequence.begin(); it != output.sequence.end(); it++ )
        cout << it->value.dbl() << ",";

    cout << "],";
    cout << output.sequence.size() << ",";
    cout << "[";

    for( it = output.sequence.begin(); it != output.sequence.end(); it++ )
        cout << it->time << ",";

    cout << "],";
    cout << output.sequence.back().time << ",";
    cout << output.beta.dbl() << ",";
    cout << output.lambda.dbl() << ",";
    cout << output.sigma.dbl() << ",";
    cout << abs( output.fbeta.dbl() ) << ";";
}

void perfTable( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args ) {
    ostringstream newtonRow;
    ostringstream bisectionRow;
    ostringstream filesRow;
    ostringstream bisectionRowIter;
    ostringstream newtonRowIter;
    ostringstream newtonPrecisionRow;
    ostringstream bisectionPrecisionRow;
    ostringstream separadores;
    newtonRow.precision( 4 );
    bisectionRow.precision( 4 );
    bisectionPrecisionRow.precision( 2 );
    newtonPrecisionRow.precision( 2 );

    input.x0 = args.x0;
    input.precision = 12;
    input.tolerance = 1e-6;
    input.useLogFunction = false;

    MetnumMethodOutput o1  = runMetnumBisectionMethod( input );
    MetnumMethodOutput o2  = runMetnumNewtonMethod( input );

    filesRow       << "&" << fileName;
    newtonRow      << "&" << o2.sequence.back().time / 1000 << "ms";
    bisectionRow   << "&" << o1.sequence.back().time / 1000 << "ms";
    separadores    << "|c";
    newtonRowIter    << "&" << o2.sequence.size();
    bisectionRowIter << "&" << o1.sequence.size();
    newtonPrecisionRow  << "&" << abs( o2.fbeta.dbl() );
    bisectionPrecisionRow  << "&" << abs( o1.fbeta.dbl() );

    cout.precision( 2 );
    cout << "\\begin{tabular}{| l |" << separadores.str() << "|}" << endl;
    cout << "\\hline" << endl;
    cout << "Archivo" << filesRow.str() << "\\\\" << endl;
    cout << "\\hline" << endl;
    cout << "T. Newton"     << newtonRow.str() << "\\\\" << endl;
    cout << "T. Bisección" << bisectionRow.str() << "\\\\" << endl;
    cout << "\\#iter Newton"    << newtonRowIter.str() << "\\\\" << endl;
    cout << "\\#iter Bisección" << bisectionRowIter.str() << "\\\\" << endl;
    cout << "$|f(0)|$ Newton" << newtonPrecisionRow.str() << "\\\\" << endl;
    cout << "$|f(0)|$ Biseccion" << bisectionPrecisionRow.str() << "\\\\" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}" << endl;
    cout << endl;
}

void computeParameters( const string& fileName, MetnumMethodInput& input, MetnumCmdArgs& args ) {

    //busca el primer x donde f(x) > 0 para asegurar que x0 esté a la derecha del cero
    input.x0 = TFloat( 3, 52 );

    while( input.functor.computeF( input.x0 ).dbl() < 0 ) input.x0 = input.x0 + 1.0;

    input.precision = 52;
    input.tolerance = 10e-8;
    input.useLogFunction = true;

    cout << fileName << endl;
    cout.precision( 10 );

    MetnumMethodOutput output = runMetnumBisectionMethod( input );
    cout << "Parámetros calculados con el método de bisección: " << endl;
    cout << fixed << "beta = " << output.beta.dbl() << endl;
    cout << fixed << "lambda = " << output.lambda.dbl() << endl;
    cout << fixed << "sigma = " << output.sigma.dbl() << endl;
    cout << endl;

    output = runMetnumNewtonMethod( input );
    cout << "Parámetros calculados con el método de Newton: " << endl;
    cout << fixed << "beta = " << output.beta.dbl() << endl;
    cout << fixed << "lambda = " << output.lambda.dbl() << endl;
    cout << fixed << "sigma = " << output.sigma.dbl() << endl;
    cout << endl;
}

double stringToDouble( string& str ) {
    istringstream ss( str );
    double value;
    ss >> value;

    return value;
}

