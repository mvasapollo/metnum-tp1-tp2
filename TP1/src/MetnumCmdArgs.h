#ifndef __METNUM_CMD_ARGS_H__
#define __METNUM_CMD_ARGS_H__

#include <iostream>
#include <vector>
using namespace std;

#define CMD_ARG_NOT_SET     (-1)

struct MetnumCmdArgs {
    MetnumCmdArgs() : x0( CMD_ARG_NOT_SET ), prec( CMD_ARG_NOT_SET ),
        tol( CMD_ARG_NOT_SET ), logf( CMD_ARG_NOT_SET ), exp( CMD_ARG_NOT_SET ),
        perf( CMD_ARG_NOT_SET ) {}

    double x0;
    int prec;
    int tol;
    int logf;
    int exp;
    int perf;
    vector<string> fileNames;
};

bool parseMetnumCmdArgs( int argc, char** argv, MetnumCmdArgs& args );
void printHelp( string programName );

#endif //__COMMAND_LINE_ARGUMENTS_H__