#include "MetnumCmdArgs.h"

#include <iostream>
#include <sstream>
using namespace std;

bool stringToInt( string& str, int& value ) {
    istringstream ss( str );

    return ss >> value;
}

bool stringToDouble( string& str, double& value ) {
    istringstream ss( str );

    return ss >> value;
}

bool parseMetnumCmdArgs( int argc, char** argv, MetnumCmdArgs& args ) {
    for( int i = 1; i < argc; ++i ) {

        string arg( argv[i] ), value;

        if( arg == "-x0" ) {
            if( i + 1 == argc || !stringToDouble( ( value = argv[++i] ), args.x0 ) ) return false;

        } else if( arg == "-prec" ) {
            if( i + 1 == argc || !stringToInt( ( value = argv[++i] ), args.prec ) ) return false;

        } else if( arg == "-tol" ) {
            if( i + 1 == argc || !stringToInt( ( value = argv[++i] ), args.tol ) ) return false;

        } else if( arg == "-logf" ) {
            args.logf = 1;

        } else if( arg == "-exportar" ) {
            args.exp = 1;

        } else if( arg == "-tabular" ) {
            args.perf = 1;

        } else {
            if( arg[0] == '-' || ( '0' <= arg[0] && arg[0] <= '9' ) ) return false;

            args.fileNames.push_back( arg );
        }
    }

    if( args.fileNames.size() == 0 ) return false;

    if( args.perf == 1 && args.x0 == CMD_ARG_NOT_SET ) return false;

    return true;
}

void printHelp( string programName ) {
    cout << endl << endl;
    cout << "Modo de uso: " << programName << " [-opción] [-parámetro valor] <nombre_de_archivo_1> <nombre_de_archivo_2> ..." << endl;
    cout << "Las opciones son las siguientes: " << endl;
    cout << "\t-logf\t\tUtilizar la función con el logaritmo aplicado para hallar beta" << endl;
    cout << "\t-exportar\tExporta a formato Matlab/Octave" << endl;
    cout << "\t-tabular\tTabular [requiere que se defina el parámetro x0]" << endl;
    cout << "Los parámetros son los siguientes: " << endl;
    cout << "\t-x0\t\tAproximación inicial del cero [decimal con coma]" << endl;
    cout << "\t-prec\t\tPrecisión a utilizar en el tipo flotante [entero positivo menor a 53]" << endl;
    cout << "\t-tol\t\tTolerancia en cantidad de dígitos [entero positivo]" << endl;
}