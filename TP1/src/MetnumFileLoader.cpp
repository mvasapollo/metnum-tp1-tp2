#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

#define ERRORMSG_FILE_NOT_FOUND(str) ("El archivo \"" + string(str) + "\" no fue encontrado")
#define ERRORMSG_FILE_INVALID(str)   ("El archivo \"" + string(str) + "\" es inválido")

vector<double> loadFile( string filename ) {

    int cantValores;
    vector<double> values;
    double d;
    ifstream inputFile( filename.c_str() );

    if(!inputFile.is_open())
    	throw ERRORMSG_FILE_NOT_FOUND(filename);

    string line;
    getline( inputFile, line );
    istringstream iss( line );
    if(!( iss >> cantValores ))
        throw ERRORMSG_FILE_INVALID(filename);

    getline( inputFile, line );
    istringstream iss2( line );
    while( iss2 >> d ) {
        values.push_back( d );
    }
    inputFile.close();
    
    return values;
}