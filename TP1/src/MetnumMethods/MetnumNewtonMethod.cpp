#include <vector>
#include <cmath>
#include <time.h>
#include <sys/time.h>

#include "MetnumMethod.h"
#include "MetnumNewtonMethod.h"
#include "MetnumFunctor.h"
#include "MetnumTimer.h"

#define MAX_ITERATIONS 100

MetnumMethodOutput runMetnumNewtonMethod( MetnumMethodInput& input ) {

    //set up functor
    MetnumFunctor& functor = input.functor;
    functor.setPrecision( input.precision );
    functor.setUseLogFunction( input.useLogFunction );

    //init sequence
    vector<TupleTimeValue> sequence;
    sequence.push_back( TupleTimeValue( 0, input.x0 ) );

    //set up timer
    MetnumTimer timer; timer.begin();

    //run method
    TFloat x = input.x0, fx, gx;

    for( int it = 0; it < MAX_ITERATIONS; ++it ) {

        functor.computeFandG( x, fx, gx );

        if( abs( fx.dbl() ) < input.tolerance ) break;

        x = gx;

        sequence.push_back( TupleTimeValue( timer.getTimeDiff(), x ) );
    }

    TFloat beta, lambda, sigma, fbeta;
    beta = x;
    functor.computeLambdaAndSigma( beta, lambda, sigma );
    fbeta = fx;

    return MetnumMethodOutput( sequence, beta, lambda, sigma, fbeta );
}