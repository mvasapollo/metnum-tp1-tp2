#include <cmath>
#include <vector>
using namespace std;

#include "MetnumFunctor.h"

/* Precomputa en un mismo ciclo (para cada dato d(i) de la entrada):
     log(d(i))              { logs.at(i) }
     [log(d(i))]^2          { logs_pow_2.at(i) }
 */
MetnumFunctor::MetnumFunctor( vector<double>& data, size_t precision, bool useLogFunction ) :
		data( data ), precision( precision ), useLogFunction( useLogFunction ) {
    n = data.size();
    lastX = -100;

    for( vector<double>::const_iterator i = data.begin(); i != data.end(); ++i ) {
        double di = *i;
        double logdi = log( di );
        logs.push_back( logdi );
        logs_pow_2.push_back( logdi * logdi );
    }
}

void MetnumFunctor::setPrecision( size_t precision ) {
    this->precision = precision;
}

void MetnumFunctor::setUseLogFunction( bool useLogFunction ) {
    this->useLogFunction = useLogFunction;
}

/* Computa F (o L) en términos de las funciones auxiliares */
TFloat MetnumFunctor::computeF( TFloat& x ) {
    computeAuxiliaryFunctions( x );

    TFloat fx;

    if( useLogFunction ) {
        fx = TFloat::log( M_2x / M_x_pow_2 ) - TFloat::log( 1 + x * ( R_x - R_0 ) );

    } else {
        fx = M_2x / M_x_pow_2 - ( 1 + x * ( R_x - R_0 ) );
    }

    return fx;
}

/* Computa F (o L) y G en términos de las funciones auxiliares */
void MetnumFunctor::computeFandG( TFloat& x, TFloat& fx, TFloat& gx ) {
    computeAuxiliaryFunctions( x );

    fx = computeF( x );
    TFloat dfx;

    if( useLogFunction ) {
        dfx = ( 2 * Mp_2x ) / M_2x + ( R_x - R_0 + x * Rp_x ) / ( x * R_0 - x * R_x - 1 )
        		- ( 2 * Mp_x / M_x );

    } else {
        dfx = R_0 - R_x - ( 2 * M_2x * Mp_x ) / M_x_pow_3 + ( 2 * Mp_2x ) / M_x_pow_2 - x * Rp_x;
    }

    gx = x - fx / dfx;
}

/* Computa lambda y sigma en términos de las funciones auxiliares */
void MetnumFunctor::computeLambdaAndSigma( TFloat& x, TFloat& lambda, TFloat& sigma ) {
    computeAuxiliaryFunctions( x );

    lambda = 1 / ( x * ( R_x - R_0 ) );
    sigma = TFloat(pow( (M_x * x * ( R_x - R_0 )).dbl() , 1 / x.dbl() ), precision);
}

/* Computa en un mismo ciclo:
    M(x)              { M_x }           |    M'(2x)            { Mp_2x }
    M(x)^2            { M_x_pow_2 }     |    M''(x)            { Mpp_x }
    M(x)^3            { M_x_pow_3 }     |    R(0)              { R_0 }
    M(2x)             { M_2x }          |    R(x)              { R_x }
    M'(x)             { Mp_x }          |    R'(x)             { Rp_x }
 */
void MetnumFunctor::computeAuxiliaryFunctions( TFloat& x ) {
    if( lastX == x ) return;
    lastX = x;

    double di, logdi, logdi_pow_2, di_pow_x, di_pow_2x;
    M_x = TFloat(0, precision);
    M_2x = TFloat(0, precision);
    Mp_x = TFloat(0, precision);
    Mp_2x = TFloat(0, precision);
    Mpp_x = TFloat(0, precision);
    R_0 = TFloat(0, precision);

    for( int i = 0; i < n; ++i ) {
        di = data.at( i );
        di_pow_x = pow( di, x.dbl() );
        di_pow_2x = di_pow_x * di_pow_x;
        logdi = logs.at( i );
        logdi_pow_2 = logs_pow_2.at( i );

        M_x = M_x + di_pow_x;
        M_2x = M_2x + di_pow_2x;
        Mp_x = Mp_x + di_pow_x * logdi;
        Mp_2x = Mp_2x + di_pow_2x * logdi;
        Mpp_x = Mpp_x + di_pow_x * logdi_pow_2;
        R_0 = R_0 + logdi;
    }

    M_x = M_x / n;
    M_x_pow_2 = M_x * M_x;
    M_x_pow_3 = M_x * M_x_pow_2;
    M_2x = M_2x / n;
    Mp_x = Mp_x / n;
    Mp_2x = Mp_2x / n;
    Mpp_x = Mpp_x / n;
    R_0 = R_0 / n;
    R_x = Mp_x / M_x;
    Rp_x = ( M_x * Mpp_x - Mp_x * Mp_x ) / ( M_x * M_x );
}
