#ifndef __METNUM_METHOD_H__
#define __METNUM_METHOD_H__

#include <sys/time.h>
#include <vector>
using namespace std;

#include "../TFloat/TFloat.h"
#include "MetnumFunctor.h"

struct TupleTimeValue {
    TupleTimeValue( double time, TFloat value ) : value( value ), time( time ) {}

    TFloat value;
    double time;
};

struct MetnumMethodInput {
    MetnumMethodInput( vector<double>& values ) : functor( MetnumFunctor( values ) ) {}

    MetnumFunctor functor;
    TFloat x0;
    size_t precision;
    double tolerance;
    bool useLogFunction;
};

struct MetnumMethodOutput {
    MetnumMethodOutput() {}
    MetnumMethodOutput( vector<TupleTimeValue>& sequence, TFloat& beta, TFloat& lambda, TFloat& sigma, TFloat& fbeta )
        : sequence(sequence), beta(beta), lambda(lambda), sigma(sigma), fbeta(fbeta) {}

    vector<TupleTimeValue> sequence;
    TFloat beta;
    TFloat lambda;
    TFloat sigma;
    TFloat fbeta;
};

#endif //__METNUM_METHOD_H__
