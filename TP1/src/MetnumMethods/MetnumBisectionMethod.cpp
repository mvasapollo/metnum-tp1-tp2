#include <vector>
#include <sstream>
#include <cmath>
#include <time.h>
#include <sys/time.h>
#include <cstdlib>

#include "MetnumMethod.h"
#include "MetnumBisectionMethod.h"
#include "MetnumFunctor.h"
#include "MetnumTimer.h"

#define MAX_ITERATIONS 100

#define INTERVAL_LOWER_BOUND (0.1)

MetnumMethodOutput runMetnumBisectionMethod( MetnumMethodInput& input ) {

    //set up functor
    MetnumFunctor& functor = input.functor;
    functor.setPrecision( input.precision );
    functor.setUseLogFunction( input.useLogFunction );

    //init sequence
    vector<TupleTimeValue> sequence;
    sequence.push_back( TupleTimeValue( 0, input.x0 ) );

    //set up timer
    MetnumTimer timer; timer.begin();

    //run method
    TFloat x, fx;
    TFloat xl = TFloat( INTERVAL_LOWER_BOUND, input.precision );
    TFloat fxl = functor.computeF( xl ), xh = input.x0;

    for( int it = 0; it < MAX_ITERATIONS; ++it ) {
        x = ( xl + xh ) * 0.5;
        fx = functor.computeF( x );

        if( abs( fx.dbl() ) < input.tolerance ) break;
        if( ( fxl * fx ).dbl() < 0 ) {
            xh = x;

        } else {
            xl = x;
            fxl = fx;
        }
        sequence.push_back( TupleTimeValue( timer.getTimeDiff(), x ) );
    }

    TFloat beta, lambda, sigma, fbeta;
    beta = x;
    functor.computeLambdaAndSigma( beta, lambda, sigma );
    fbeta = fx;

    return MetnumMethodOutput( sequence, beta, lambda, sigma, fbeta );
}
