#ifndef __METNUM_TIMER_H__
#define __METNUM_TIMER_H__

#include <ctime>
#include <sys/time.h>

class MetnumTimer
{
public:
	void begin();
	double getTimeDiff();

private:
	timespec t0;
};

#endif //__METNUM_TIMER_H__