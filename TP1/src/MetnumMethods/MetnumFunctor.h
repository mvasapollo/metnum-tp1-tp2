#ifndef __METNUM_FUNCTOR_H__
#define __METNUM_FUNCTOR_H__

#include <vector>
using namespace std;

#include "../TFloat/TFloat.h"

class MetnumFunctor
{
public:
	MetnumFunctor(vector<double>& data, size_t precision = 52, bool useLogFunction = false);
	
	void setPrecision(size_t precision);
	void setUseLogFunction(bool useLogFunction);

	/* Computan L, F, G, lambda o sigma en términos de las funciones auxiliares */
	TFloat computeF( TFloat& xtf );
	void computeFandG( TFloat& xtf, TFloat& fx, TFloat& gx );
	void computeLambdaAndSigma( TFloat& xtf, TFloat& lambda, TFloat& sigma );

private:
	/* Computa en un mismo ciclo:
	    M(x)              { M_x }			|    M'(2x)            { Mp_2x }
	    M(x)^2            { M_x_pow_2 }		|    M''(x)            { Mpp_x }
	    M(x)^3            { M_x_pow_3 }		|    R(0)              { R_0 }
	    M(2x)             { M_2x }			|    R(x)              { R_x }
	    M'(x)             { Mp_x }			|    R'(x)             { Rp_x }
	 */
	void computeAuxiliaryFunctions( TFloat& x );
	
	size_t precision;
	bool useLogFunction;

	int n;
	vector<double>& data;
	vector<double> logs;
	vector<double> logs_pow_2;
	TFloat lastX;

	/* guardan el valor de las funciones auxiliares */
	TFloat M_x;
	TFloat M_x_pow_2;
	TFloat M_x_pow_3;
	TFloat M_2x;
	TFloat Mp_x;
	TFloat Mp_2x;
	TFloat Mpp_x;
	TFloat R_0;
	TFloat R_x;
	TFloat Rp_x;
};

#endif //__METNUM_FUNCTOR_H__