#include "MetnumTimer.h"

#include <ctime>
#include <sys/time.h>

#define SEC_TO_USEC     (1e6)
#define NSEC_TO_USEC    (1e-3)

void MetnumTimer::begin()
{
	clock_gettime( CLOCK_THREAD_CPUTIME_ID,  &t0 );
}

double MetnumTimer::getTimeDiff()
{
	timespec timeCurrent;
    clock_gettime( CLOCK_THREAD_CPUTIME_ID,  &timeCurrent );

    return ( timeCurrent.tv_sec - t0.tv_sec ) * SEC_TO_USEC + ( double )( timeCurrent.tv_nsec - t0.tv_nsec ) * NSEC_TO_USEC;
}