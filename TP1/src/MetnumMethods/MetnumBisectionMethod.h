#ifndef __METNUM_BISECTION_METHOD_H__
#define __METNUM_BISECTION_METHOD_H__

#include "MetnumMethod.h"

MetnumMethodOutput runMetnumBisectionMethod( MetnumMethodInput& input );

#endif //__METNUM_BISECTION_METHOD_H__