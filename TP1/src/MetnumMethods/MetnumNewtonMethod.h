#ifndef __NEWTON_METHOD_H__
#define __NEWTON_METHOD_H__

#include "MetnumMethod.h"

MetnumMethodOutput runMetnumNewtonMethod( MetnumMethodInput& input );

#endif //__NEWTON_METHOD_H__