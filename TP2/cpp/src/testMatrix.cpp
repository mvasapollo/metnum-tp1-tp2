#include <iostream>
#include <cstdlib>
#include <ctime>

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"
using namespace std;

int main() {

	srand(time(0));


	MMMatrix a(MMMatrixUtils::rand(5,5));
	// a.permutateR(0,3);
	// PRINT_EXPR(MMMatrixUtils::permutationMatrix(a));
	// PRINT_EXPR(a);
	// a.tInPlace();
	// PRINT_EXPR(a);

	// unsigned N = 100;
	// MMMatrix A = MMMatrixUtils::rand(N,N,-10,10);

	// MMLU lu(A);
	// PRINT_EXPR(A);
	// PRINT_MSG_EXPR("L", lu.L);
	// PRINT_MSG_EXPR("U", lu.U);
	// PRINT_MSG_EXPR("P", MMMatrixUtils::permutationMatrix(lu.U));

	// //1D
	// MMVector b = MMMatrixUtils::rand(N,1,-5,5);
	// MMVector x = lu.solve(b);
	// PRINT_EXPR(b);
	// PRINT_EXPR(x);

	//2D
	// MMMatrix B = MMMatrixUtils::rand(N,N,-5,5);
	// MMMatrix X = lu.solve(B);
	// PRINT_EXPR(B);
	// PRINT_EXPR(X);


	// MMMatrix randm = MMMatrixUtils::rand(5,5,0,10);
	// MMLU lu(randm);

	// PRINT_EXPR(randm);
	// PRINT_EXPR(lu);
	
// 	PRINT_MSG("Ceros: ");
// 	MMMatrix zeros = MMMatrixUtils::zeros(5);
// 	PRINT_EXPR(zeros);

// 	PRINT_MSG("Identidad: ");
// 	MMMatrix id = MMMatrixUtils::identity(5);
// 	PRINT_EXPR(id);

	// PRINT_MSG("Rand: ");
	// MMMatrix randvec = MMMatrixUtils::rand(5,1,0,1);
	// PRINT_EXPR(randvec);

	// PRINT_MSG("Vector: ");
	// MMMatrix vec = MMMatrix(5,0.0);
	// for (int i = 0; i < vec.size(); ++i)
	// 	vec(i) = i;
	// PRINT_EXPR(vec);
	// PRINT_EXPR(vec.t());
	// MMMatrix lala(((((MMMatrixUtils::cos((vec * vec.t()) + MMMatrixUtils::identity(vec.size())))*MMMatrixUtils::linvector(5,0,10) + 1)*100).permutateR(0,2)));
	// lala = lala.t().t() * 10;
	// PRINT_EXPR(lala - 10000);
	// PRINT_EXPR(lala.col(0));
	// PRINT_MSG("Vector lineal: ");
	// MMMatrix linvec = MMMatrixUtils::linvector(5,0,3.14159);
	// PRINT_EXPR(linvec);
	// PRINT_EXPR(MMMatrixUtils::range(linvec));
	// MMMatrix prod = randvec * linvec.t();
	// PRINT_EXPR(prod);

// 	PRINT_MSG("Aplicar coseno elemento a elemento: ");
// 	MMMatrix cosM = MMMatrixUtils::cos(linvec);
// 	PRINT_EXPR(cosM);

// 	PRINT_MSG("Test 1:");
// 	MMMatrix m = MMMatrix::square(3);
// 	m(0, 0) = 2;
// 	m(1, 0) = 1;
// 	m(2, 0) = 4;

// 	m(0, 1) = 1;
// 	m(1, 1) = 2;
// 	m(2, 1) = 1;

// 	m(0, 2) = 1;
// 	m(1, 2) = 2;
// 	m(2, 2) = 2;

// //// ================================

// 	MMMatrix m2 = MMMatrix::square(3);
// 	m2(0, 0) = 1.4090;
// 	m2(1, 0) = 1.4172;
// 	m2(2, 0) = 0.6715;

// 	m2(0, 1) = -1.2075;
// 	m2(1, 1) = 0.7172;
// 	m2(2, 1) = 1.6302;

// 	m2(0, 2) = 0.4889;
// 	m2(1, 2) = 1.0347;
// 	m2(2, 2) = 0.7269;

// //	 1.4090   -1.2075    0.4889
// //	 1.4172    0.7172    1.0347
// //	 0.6715    1.6302    0.7269

// //// ================================
// //
// 	MMMatrix m3 = MMMatrix::square(3);
// 	m3(0, 0) = -0.3034;
// 	m3(1, 0) = 0.2939;
// 	m3(2, 0) = -0.7873;

// 	m3(0, 1) = 0.8884;
// 	m3(1, 1) = -1.1471;
// 	m3(2, 1) = -1.0689;

// 	m3(0, 2) = -0.8095;
// 	m3(1, 2) = -2.9443;
// 	m3(2, 2) = 1.4384;

// ////	   -0.3034    0.8884   -0.8095
// ////	    0.2939   -1.1471   -2.9443
// ////	   -0.7873   -1.0689    1.4384
// //
// //// ================================
// //
// 	PRINT_MSG("Test 1:");
// 	PRINT_MSG_EXPR("A",m);
// 	MMLU lu(m);
// 	PRINT_EXPR(lu);


// 	PRINT_MSG("Test 2:");
// 	PRINT_MSG_EXPR("A",m2);
// 	MMLU lu2(m2);
// 	PRINT_EXPR(lu2);

// 	PRINT_MSG("Test 3:");
// 	PRINT_MSG_EXPR("A",m3);
// 	MMLU lu3(m3);
// 	PRINT_EXPR(lu3);

// 	PRINT_MSG("Suma elemento a elemento: ");
// 	MMMatrix Msuma = m2 | m3;
// 	PRINT_EXPR(Msuma);

// 	PRINT_MSG("Producto elemento a elemento: ");
// 	MMMatrix Mprod = m2 & m3;
// 	PRINT_EXPR(Mprod);



// 	// VISTA: ==========================================
// 	MMMatrix v1 = MMMatrix::view(m2, 1, 1, 2);
// 	cout << "VISTA 1" << endl;
// 	cout << v1 << endl;



	return 0;
}
