#include "cmd-args.h"

#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/tclap/CmdLine.h"
using namespace TCLAP;

#include "noise-generation.h"
#include "noise-reduction.h"

MCCmdArgs1D parseCmdArgs1D(int argc, char** argv)
{
	try {  
	
		CmdLine cmd("Metum TP2: reducción de ruido en señales", ' ', "0.1");

		// Define a value argument and add it to the command line.
		ValueArg<double> nr_level("","nivelf","Nivel de reducción de ruido",false,0.1,"double");
		cmd.add( nr_level );
		
		// Define a value argument and add it to the command line.
		vector<string> allowed_nr;
		allowed_nr.push_back(NR_THRESHOLD);
		allowed_nr.push_back(NR_QUANTILE);
		allowed_nr.push_back(NR_SPECTRAL);
		ValuesConstraint<string> allowed_nr_args( allowed_nr );
		ValueArg<string> nr_fun("","filtrado","Función reductora de ruido",false,NR_THRESHOLD, &allowed_nr_args);
		cmd.add( nr_fun );

		// Define a value argument and add it to the command line.
		ValueArg<double> ng_level("","nivelr","Nivel de generación de ruido",false,0.3,"double");
		cmd.add( ng_level );

		// Define a value argument and add it to the command line.
		vector<string> allowed_ng;
		allowed_ng.push_back(NG_ADDITIVE);
		allowed_ng.push_back(NG_SPECKLE);
		allowed_ng.push_back(NG_SALTNPEPPER);
		ValuesConstraint<string> allowed_ng_args( allowed_ng );
		ValueArg<string> ng_fun("","ruido","Función generadora de ruido",false,NG_ADDITIVE,&allowed_ng_args);
		cmd.add( ng_fun );

		UnlabeledMultiArg<string> inputFilenames("inputFilenames", "Archivos de datos, separados por espacios. Los resultados se escribirán en el mismo directorio donde se encuentran los archivos de entrada.", true, "archivo");
		cmd.add( inputFilenames );

		// Parse the args.
		cmd.parse( argc, argv );
	
		// Get the value parsed by each arg.
		MCCmdArgs1D args;
		args.inputFilenames = inputFilenames.getValue();
		args.ng_fun = ng_fun.getValue();
		args.ng_level = ng_level.getValue();
		args.nr_fun = nr_fun.getValue();
		args.nr_level = nr_level.getValue();
		
		return args;
	
	} catch (ArgException &e)  // catch any exceptions
	{ DISPLAY_ERROR_AND_EXIT(e.error() + " en el argumento: " + e.argId()); }
}

MCCmdArgs2D parseCmdArgs2D(int argc, char** argv)
{
	try {  
	
		CmdLine cmd("Metum TP2: reducción de ruido en imágenes", ' ', "0.1");

		// Define a value argument and add it to the command line.
		ValueArg<int> block_size("","tambloque","Tamaño de bloque para subdividir la imágen (debe ser consistente con el tamaño de la imagen)",false,0,"int");
		cmd.add( block_size );

		// Define a value argument and add it to the command line.
		ValueArg<double> nr_level("","nivelf","Nivel de reducción de ruido",false,0.1,"double");
		cmd.add( nr_level );
		
		// Define a value argument and add it to the command line.
		vector<string> allowed_nr;
		allowed_nr.push_back(NR_THRESHOLD);
		allowed_nr.push_back(NR_QUANTILE);
		allowed_nr.push_back(NR_SPECTRAL);
		ValuesConstraint<string> allowed_nr_args( allowed_nr );
		ValueArg<string> nr_fun("","filtrado","Función reductora de ruido",false,NR_THRESHOLD, &allowed_nr_args);
		cmd.add( nr_fun );

		// Define a value argument and add it to the command line.
		ValueArg<double> ng_level("","nivelr","Nivel de generación de ruido",false,0.3,"double");
		cmd.add( ng_level );

		// Define a value argument and add it to the command line.
		vector<string> allowed_ng;
		allowed_ng.push_back(NG_ADDITIVE);
		allowed_ng.push_back(NG_SPECKLE);
		allowed_ng.push_back(NG_SALTNPEPPER);
		ValuesConstraint<string> allowed_ng_args( allowed_ng );
		ValueArg<string> ng_fun("","ruido","Función generadora de ruido",false,NG_ADDITIVE,&allowed_ng_args);
		cmd.add( ng_fun );

		UnlabeledMultiArg<string> inputFilenames("inputFilenames", "Archivos de imágenes tipo .pgm, separados por espacios. Los resultados se escribirán en el mismo directorio donde se encuentran los archivos de entrada.", true, "archivo .pgm");
		cmd.add( inputFilenames );

		// Parse the args.
		cmd.parse( argc, argv );
	
		// Get the value parsed by each arg.
		MCCmdArgs2D args;
		args.inputFilenames = inputFilenames.getValue();
		args.ng_fun = ng_fun.getValue();
		args.ng_level = ng_level.getValue();
		args.nr_fun = nr_fun.getValue();
		args.nr_level = nr_level.getValue();
		args.block_size = block_size.getValue();
		
		return args;
	
	} catch (ArgException &e)  // catch any exceptions
	{ DISPLAY_ERROR_AND_EXIT(e.error() + " en el argumento: " + e.argId()); }
}