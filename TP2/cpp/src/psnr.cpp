#include "psnr.h"

#include <cmath>	/* log10 */
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

double computePSNR1D(MMVector& x, MMVector& x_app)
{
	int N = x.size();

	double ECM = 1.0/N * MMMatrixUtils::sum(MMMatrixUtils::square((x - x_app)));
	double range = MMMatrixUtils::range(x);
	double PSNR = 10 * log10( range * range / ECM );

	return PSNR;
}

double computePSNR2D(MMMatrix& x, MMMatrix& x_app)
{
	double ECM = 1.0/(x.rows() * x.cols()) * MMMatrixUtils::sum(MMMatrixUtils::square((x - x_app)));
	double PSNR = 10 * log10( 255 * 255 / ECM );

	return PSNR;
}