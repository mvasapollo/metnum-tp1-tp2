#ifndef __PSNR__
#define __PSNR__

#include <iostream>
using namespace std;

#include "../lib/metnum-math/metnum-math.h"

double computePSNR1D(MMVector& x, MMVector& x_app);

double computePSNR2D(MMMatrix& x, MMMatrix& x_app);

#endif // __PSNR__