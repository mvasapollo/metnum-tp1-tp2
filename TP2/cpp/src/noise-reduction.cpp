#include "noise-reduction.h"

#include <cmath>	/* abs */
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

#define		ERROR_MSG_NR_LEVEL(nr)		("El parámetro que mide el nivel de filtrado debe estar en el rango [0,1), nivelf: " + int2str(nr))

MMMatrix threshold(MMMatrix& y, double nr_level)
{
	double threshold = nr_level * (MMMatrixUtils::max(MMMatrixUtils::abs(y)));
	
	MMMatrix y_nr(y.rows(), y.cols());
	MM_MAP_IJ(y_nr, (abs(y(i,j)) < threshold)? 0 : y(i,j));
	
	return y_nr;
}

MMMatrix quantile(MMMatrix& y, double nr_level) {
	MMMatrix y_sorted(y);
	MMMatrixUtils::sortElementsByAbs(y_sorted);
	int quantile = nr_level * y.rows() * y.cols() + 0.5;
	double quantileValue = y_sorted( quantile );

	MMMatrix y_nr(y);
	MM_MAP_IJ(y_nr, ((std::abs(y_nr(i,j)) < quantileValue)?0:y_nr(i,j)) );

	return y_nr;
}

MMVector spectral1D(MMVector& y, double nr_level) {

	double total = MMMatrixUtils::sum(MMMatrixUtils::abs(y));

	MMVector y_nr(y.size(), 0.0);
	double consumed = 0;
	MM_WALK_I(y_nr,
		y_nr(i) = y(i);
		consumed += abs(y(i));
		if((1 - nr_level) * total <= consumed) break;
	);

	return y_nr;
}

MMMatrix spectral2D(MMMatrix& y, double nr_level) {

	double total = MMMatrixUtils::sum(MMMatrixUtils::abs(y));

	MMMatrix y_nr(MMMatrixUtils::zeros(y.rows()));
	double consumed = 0;
	for (int i = 0; i < y.rows(); ++i) {
		for (int j = i; j >= 0; --j) {
			y_nr(i-j,i) = y(i-j,i);
			consumed += std::abs(y(i-j,i));
			if( j != 0 ) {
				y_nr(i,i-j) = y(i,i-j);
				consumed += std::abs(y(i,i-j));
			}
			if((1 - nr_level) * total <= consumed) break;
		}
	}

	return y_nr;
}

MMVector callNoiseReducingFunction1D(MMVector& y, string nr_function, double nr_level) {
	if( OUT_OF_RANGE(0,nr_level,1) )
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_NR_LEVEL(nr_level))
	if( nr_function == NR_THRESHOLD ) return threshold(y, nr_level);
	else if( nr_function == NR_QUANTILE ) return quantile(y, nr_level);
	else if( nr_function == NR_SPECTRAL ) return spectral1D(y, nr_level);
	else DISPLAY_ERROR_AND_EXIT("Función reductora de ruido inválida: " + nr_function);
}

MMMatrix callNoiseReducingFunction2D(MMMatrix& y, string nr_function, double nr_level) {
	if( OUT_OF_RANGE(0,nr_level,1) )
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_NR_LEVEL(nr_level))
	if( nr_function == NR_THRESHOLD ) return threshold(y, nr_level);
	else if( nr_function == NR_QUANTILE ) return quantile(y, nr_level);
	else if( nr_function == NR_SPECTRAL ) return spectral2D(y, nr_level);
	else DISPLAY_ERROR_AND_EXIT("Función reductora de ruido inválida: " + nr_function);
}