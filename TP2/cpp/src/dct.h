#ifndef __DCT__
#define __DCT__

#include <iostream>
using namespace std;

#include "../lib/metnum-math/metnum-math.h"

void reduceNoiseDCT1D(MMVector& x, string nr_function, double nr_level, MMVector& x_nr, MMVector& y, MMVector& y_nr);

void reduceNoiseDCT2D(MMMatrix& x, string nr_function, double nr_level, int block_size, MMMatrix& x_nr, MMMatrix& y, MMMatrix& y_nr);

#endif // __DCT__