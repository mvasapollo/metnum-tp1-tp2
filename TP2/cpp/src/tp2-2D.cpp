#include <cstdlib>		/* rand, srand */
#include <ctime>		/* time */
#include <vector>
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

#include "cmd-args.h"
#include "data-IO.h"
#include "noise-generation.h"
#include "dct.h"
#include "noise-reduction.h"
#include "psnr.h"

int main(int argc, char** argv)
{
	// iniciar el generador de números aleatorios
	srand(0);

	// obtener parámetros
	MCCmdArgs2D args = parseCmdArgs2D(argc, argv);

	// procesar cada archivo
	vector<string>::const_iterator filename;
	for(filename = args.inputFilenames.begin(); filename != args.inputFilenames.end(); ++filename)
	{
		MMMatrix x = readDataFromFile2D(*filename);

		MMMatrix x_ng = callNoiseGeneratingFunction2D(x, args.ng_fun, args.ng_level);

		BEGIN_TIMER();

		// reducir los niveles de ruido mediante DCT
		MMMatrix x_nr(x.rows(),x.cols()), y(x.rows(),x.cols()), y_nr(x.rows(),x.cols());
		reduceNoiseDCT2D(x_ng, args.nr_fun, args.nr_level, args.block_size, x_nr, y, y_nr);

		int msecs = MSECS_ELAPSED();

		// calcular PSNR para los datos antes y después de reducir ruido
		double PSNR_ng = computePSNR2D(x, x_ng);
		double PSNR_nr = computePSNR2D(x, x_nr);

		// exportar resultados
		writeDataToFile2D( *filename, args, x, x_ng, x_nr, PSNR_ng, PSNR_nr, msecs);
	}

	return 0;
}