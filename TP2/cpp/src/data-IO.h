#ifndef __DATA_IO__
#define __DATA_IO__

#include <iostream>
using namespace std;

#include "../lib/metnum-math/metnum-math.h"

#include "cmd-args.h"

MMVector readDataFromFile1D(string inputFilename);

void writeDataToFile1D(string inputFilename, MCCmdArgs1D& args, MMVector& x, MMVector& x_ng, MMVector& x_nr, MMVector& y, MMVector& y_nr, double PSNR_ng, double PSNR_nr, int msecs);

MMMatrix readDataFromFile2D(string filename);

void writeDataToFile2D(string inputFilename, MCCmdArgs2D& args, MMMatrix& x, MMMatrix& x_ng, MMMatrix& x_nr, double PSNR_ng, double PSNR_nr, int msecs);

#endif // __DATA_IO__