#ifndef __METNUM_CMD__
#define __METNUM_CMD__

#include <vector>
#include <iostream>
using namespace std;

struct MCCmdArgs1D
{
	vector<string> inputFilenames;
	string ng_fun; 								// función generadora de ruido
	double ng_level; 							// parámetro de ng_fun
	string nr_fun; 								// función reductora de ruido
	double nr_level; 							// parámetro de nr_fun
};

MCCmdArgs1D parseCmdArgs1D(int argc, char** argv);

struct MCCmdArgs2D
{
	vector<string> inputFilenames;
	string ng_fun; 								// función generadora de ruido
	double ng_level; 							// parámetro de ng_fun
	string nr_fun; 								// función reductora de ruido
	double nr_level; 							// parámetro de nr_fun
	int block_size;
};

MCCmdArgs2D parseCmdArgs2D(int argc, char** argv);

#endif // __METNUM_CMD__