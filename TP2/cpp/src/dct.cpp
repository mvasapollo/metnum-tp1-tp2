#include "dct.h"

#include <cmath>		/* M_PI */
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

#include "noise-reduction.h"

MMVector transform1D(MMMatrix& M, MMVector& x);
MMVector reverseTransform1D(MMLU& LU, MMVector& y);
MMMatrix transform2D(MMMatrix& M, MMMatrix& x);
MMMatrix reverseTransform2D(MMLU& LU, MMMatrix& y);
MMMatrix computeDCTMatrix(int N);
MMMatrix& normalize(MMMatrix& x);

void reduceNoiseDCT1D(MMVector& x, string nr_function, double nr_level, MMVector& x_nr, MMVector& y, MMVector& y_nr) {
	MMMatrix M = computeDCTMatrix(x.size());
	MMLU LU(M);

	y = transform1D(M, x);

	y_nr = callNoiseReducingFunction1D(y, nr_function, nr_level);

	x_nr = reverseTransform1D(LU, y_nr);
}

MMVector transform1D(MMMatrix& M, MMVector& x) {
	return M * x;
}

MMVector reverseTransform1D(MMLU& LU, MMVector& y) {
	return LU.solve(y);
}

void reduceNoiseDCT2D(MMMatrix& x, string nr_function, double nr_level, int block_size, MMMatrix& x_nr, MMMatrix& y, MMMatrix& y_nr) {
	int N = x.rows();

	if( block_size == 0 ) block_size =  N;
	else if( N % block_size != 0 ) DISPLAY_ERROR_AND_EXIT("La imagen debe poder dividirse en bloques de igual tamaño; es decir, el tamaño de la imagen debe ser múltiplo del tamaño del bloque");

	MMMatrix M = computeDCTMatrix(block_size);
	MMLU LU(M);

	for (int ii = 0; ii < N; ii += block_size)
		for (int jj = 0; jj < N; jj += block_size)
		{
			MMMatrix x_bl = x.block(ii, jj, block_size);

			MMMatrix y_bl = transform2D(M, x_bl);

			MMMatrix y_nr_bl = callNoiseReducingFunction2D(y_bl, nr_function, nr_level);

			MMMatrix x_nr_bl = reverseTransform2D(LU, y_nr_bl);

			for (int i = 0; i < block_size; ++i)
				for (int j = 0; j < block_size; ++j)
				{
					x(i + ii,j + jj) = x_bl(i, j);
					y(i + ii,j + jj) = y_bl(i, j);
					y_nr(i + ii,j + jj) = y_nr_bl(i, j);
					x_nr(i + ii,j + jj) = x_nr_bl(i, j);
				}
		}
	normalize(x_nr);
}

MMMatrix transform2D(MMMatrix& M, MMMatrix& x) {

	MMMatrix y(M * x);
	M.tInPlace();
	y = y * M;
	M.tInPlace();

	return  y;
}

/*
	resuelve el sistema y_nr = M * x * M' en dos pasos
	1) y = M * z
	2) z = x * M' (o equivalentemente, M * x' = z')
*/
MMMatrix reverseTransform2D(MMLU& LU, MMMatrix& y) {
	MMMatrix z = LU.solve(y);
	z.tInPlace();
	MMMatrix x = LU.solve(z);
	x.tInPlace();

	return x;
}

MMMatrix computeDCTMatrix(int N) {
	MMVector unos(N,1.0);
	MMVector C(N,2.0); C(0) -= 1; C = C * (1.0/N); C = MMMatrixUtils::sqrt(C);
	MMVector tt = MMMatrixUtils::linvector(N,0.5*M_PI/N,(N-0.5)*M_PI/N);
	MMVector f = MMMatrixUtils::linvector(N,0,N-1);
	MMMatrix Mo = (C * unos.t()) & MMMatrixUtils::cos(f * tt.t());
	MMMatrix M = MMMatrixUtils::round(Mo * 128);

	return M;
}