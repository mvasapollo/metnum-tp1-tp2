#include <cstdlib>		/* rand, srand */
#include <ctime>		/* time */
#include <vector>
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

#include "cmd-args.h"
#include "data-IO.h"
#include "noise-generation.h"
#include "dct.h"
#include "noise-reduction.h"
#include "psnr.h"

int main(int argc, char** argv)
{
	// iniciar el generador de números aleatorios
	srand(0);

	// obtener parámetros
	MCCmdArgs1D args = parseCmdArgs1D(argc, argv);

	// procesar cada archivo
	vector<string>::const_iterator filename;
	for(filename = args.inputFilenames.begin(); filename != args.inputFilenames.end(); ++filename)
	{
		MMVector x = readDataFromFile1D(*filename);

		MMVector x_ng = callNoiseGeneratingFunction1D(x, args.ng_fun, args.ng_level);

		BEGIN_TIMER();

		// reducir los niveles de ruido mediante DCT
		MMVector x_nr(x.size(),0.0), y(x.size(),0.0), y_nr(x.size(),0.0);
		reduceNoiseDCT1D(x_ng, args.nr_fun, args.nr_level, x_nr, y, y_nr);

		int msecs = MSECS_ELAPSED();

		// calcular PSNR para los datos antes y después de reducir ruido
		double PSNR_ng = computePSNR1D(x, x_ng);
		double PSNR_nr = computePSNR1D(x, x_nr);

		// exportar resultados
		writeDataToFile1D( *filename, args, x, x_ng, x_nr, y, y_nr, PSNR_ng, PSNR_nr, msecs);
	}

	return 0;
}