#include "noise-generation.h"

#include <cstdlib>
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

/* 1D */

MMVector additiveNoise1D(MMVector& x, double ng_level)
{
	double std = ng_level * MMMatrixUtils::range(x);
	MMVector ns = MMMatrixUtils::rand(x.size(),1) * std;

	return x + ns;
}

MMVector speckleNoise1D(MMVector& x, double ng_level)
{
	double std = ng_level * MMMatrixUtils::range(x);
	MMVector ns = MMMatrixUtils::rand(x.size(),1) * std + 1;

	return x & ns;
}

MMMatrix saltnpepperNoise(MMMatrix& x, double ng_level) {
	double min, max;
	MMMatrixUtils::minmax(MMMatrixUtils::abs(x),min,max);

	MMMatrix xns(x);
	MM_WALK_IJ(xns,
		double p = ((double)std::rand())/RAND_MAX;
		if( ng_level < p )
			continue;
		else if( (ng_level/2) < p )
			xns(i,j) = min;
		else
			xns(i,j) = max;
	);

	return xns;
}

MMVector callNoiseGeneratingFunction1D(MMVector& x, string ng_function, double ng_level)
{
	if( ng_function == NG_ADDITIVE ) return additiveNoise1D(x, ng_level);
	else if( ng_function == NG_SPECKLE ) return speckleNoise1D(x, ng_level);
	else if( ng_function == NG_SALTNPEPPER ) return saltnpepperNoise(x, ng_level);
	else DISPLAY_ERROR_AND_EXIT("Función generadora de ruido inválida: " + ng_function);
}

/* 2D */

MMMatrix& normalize(MMMatrix& x)
{
	MM_WALK_IJ(x,
		double value = x(i,j);
		if( value < 0 ) x(i,j) = 0;
		else if ( value > 255 ) x(i,j) = 255;
	);

	return x;
}

MMMatrix additiveNoise2D(MMMatrix& x, double ng_level)
{
	double std = ng_level * 255;
	MMMatrix ns = MMMatrixUtils::rand(x.rows(),x.cols()) * std;

	MMMatrix sum(x+ns);
	return normalize(sum);
}

MMMatrix speckleNoise2D(MMMatrix& x, double ng_level)
{
	double std = ng_level * 255;
	MMMatrix ns = MMMatrixUtils::rand(x.rows(),x.cols()) * std + 1;

	MMMatrix prod(x&ns);
	return normalize(prod);
}

MMMatrix callNoiseGeneratingFunction2D(MMMatrix& x, string ng_function, double ng_level)
{
	if( ng_function == NG_ADDITIVE ) return additiveNoise2D(x, ng_level);
	else if( ng_function == NG_SPECKLE ) return speckleNoise2D(x, ng_level);
	else if( ng_function == NG_SALTNPEPPER ) return saltnpepperNoise(x, ng_level);
	else DISPLAY_ERROR_AND_EXIT("Función generadora de ruido inválida: " + ng_function);
}