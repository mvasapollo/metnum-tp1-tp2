#include "data-IO.h"

#include<fstream>
#include <iostream>
using namespace std;

#include "../lib/macros.h"
#include "../lib/metnum-math/metnum-math.h"

#include "cmd-args.h"

#define		PRINT_MATLAB_MAT(stream, mat)								\
	{ (stream) << #mat << " = [" << (mat) << "];" << endl; }			\

#define		PRINT_MATLAB_IMG(stream, im)								\
	{ (stream) << #im << " = uint8([" << (im) << "]);" << endl; }		\

#define		PRINT_MATLAB_STR(stream, str)								\
	{ (stream) << #str << " = \'" << (str) << "\';" << endl; }			\

#ifdef _WIN64
	#define		DIRECTORY_SEPARATOR		('\\')
#elif _WIN32
	#define		DIRECTORY_SEPARATOR		('\\')
#elif __linux
	#define		DIRECTORY_SEPARATOR		('/')
#endif

string changeExtension( const string& path, string extension )
{
	string prefix = "r_";
	string filename = path.substr( path.find_last_of( DIRECTORY_SEPARATOR ) + 1 );
	string directory = path.substr( 0, path.find_last_of( DIRECTORY_SEPARATOR ) + 1 );
	
	return directory + "r_" + filename.substr( 0, filename.find_last_of( '.' ) ) + extension;
}

/* 1D */

MMVector readDataFromFile1D(string filename)
{
    ifstream file;
    file.open(filename.c_str());
    if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo abrir el archivo: \"" + filename + "\"");

    int N;
	file >> N;
	if( !file )
		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");
    MMVector x(N,0.0);

    double value; int i = 0;
    while (!file.eof() && i < N)
    {
		file >> value;
		
		if( !file )
   			DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");

		x(i) = value;
		++i;
   	}

    file.close();

   	if( i != N )
   		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");

	return x;
}

void writeSignal(string output, MMVector& sig)
{
	ofstream file;
	file.open(output.c_str());
	if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo crear el archivo de salida: \"" + output + "\"");

	file << sig.size() << endl;
	MM_WALK_I(sig, file << sig(i) << " ");

	file.close();
}

void writeDataToFile1D(string inputFilename, MCCmdArgs1D& args, MMVector& x, MMVector& x_ng, MMVector& x_nr, MMVector& y, MMVector& y_nr, double PSNR_ng, double PSNR_nr, int msecs)
{
	ofstream file;
	string outputFilename = changeExtension(inputFilename, ".m");
	file.open(outputFilename.c_str());
	if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo crear el archivo de salida: \"" + outputFilename + "\"");

	string sigNgOutput = changeExtension(inputFilename, "_ng.sig");
	string sigNrOutput = changeExtension(inputFilename, "_nr.sig");

	// imprimir datos de interes por consola
	PRINT_MSG("Resultados\n------------");
	PRINT_MSG_EXPR("Señal", inputFilename);
	PRINT_MSG_EXPR("Función generadora de ruido", args.ng_fun);
	PRINT_MSG_EXPR("Nivel de ruido", args.ng_level);
	PRINT_MSG_EXPR("Función de filtrado", args.nr_fun);
	PRINT_MSG_EXPR("Nivel de filtrado", args.nr_level);
	PRINT_MSG_EXPR("Señal con ruido agregado", sigNgOutput);
	PRINT_MSG_EXPR("Señal con ruido filtrado", sigNrOutput);
	PRINT_MSG_EXPR("PSNR de la señal con ruido", PSNR_ng);
	PRINT_MSG_EXPR("PSNR de la señal después de filtrar", PSNR_nr);
	PRINT_MSG_EXPR("Tiempo de ejecución (ms)", msecs);
	PRINT_MSG(" ");

	// imprimir datos de interes a un archivo de matlab
	PRINT_MATLAB_STR(file, inputFilename);
	PRINT_MATLAB_STR(file, args.ng_fun);
	PRINT_MATLAB_MAT(file, args.ng_level);
	PRINT_MATLAB_STR(file, args.nr_fun);
	PRINT_MATLAB_MAT(file, args.nr_level);
	PRINT_MATLAB_MAT(file, x);
	PRINT_MATLAB_MAT(file, x_ng);
	PRINT_MATLAB_MAT(file, x_nr);
	PRINT_MATLAB_MAT(file, y);
	PRINT_MATLAB_MAT(file, y_nr);
	PRINT_MATLAB_MAT(file, PSNR_ng);
	PRINT_MATLAB_MAT(file, PSNR_nr);
	PRINT_MATLAB_MAT(file, msecs);

	file.close();

	writeSignal(sigNgOutput, x_ng);
	writeSignal(sigNrOutput, x_nr);
}

/* 2D, fotos .pgm */

MMMatrix readDataFromFile2D(string filename)
{
    ifstream file;
    file.open(filename.c_str());
    if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo abrir el archivo: \"" + filename + "\"");

	string p5;
	file >> p5;
	if( !file || p5 != "P5" )
		DISPLAY_ERROR_AND_EXIT("El archivo no tiene formato .pgm: \"" + filename + "\"");
	
	unsigned width, height, range;
	file >> width;
	if( !file )
		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");
	file >> height;
	if( !file )
		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");
	if( width != height )
		DISPLAY_ERROR_AND_EXIT("La imágen debe ser cuadrada: \"" + filename + "\"");
	file >> range;
	if( !file || range != 255 )
		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");

	
	char buffer[width*height];
	file.read(buffer, 1);
    file.read(buffer, width*height);
    if( !file )
		DISPLAY_ERROR_AND_EXIT("El archivo es inválido: \"" + filename + "\"");

    file.close();

	MMMatrix x(width,height);
	for (int i = 0; i < width; ++i)
		for (int j = 0; j < height; ++j)
			x(i,j) = (buffer[i*height+j] & 0xFF);

	return x;
}

void writePGM(string output, MMMatrix& im)
{
	ofstream file;
	file.open(output.c_str());
	if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo crear el archivo de salida: \"" + output + "\"");

	file << "P5 ";
	file << im.rows() << " ";
	file << im.cols() << " ";
	file << 255 << endl;

	MM_WALK_IJ(im,
		char value = ROUND(im(i,j));
		file.write(&value, 1);
	);

	file.close();
}

void writeDataToFile2D(string inputFilename, MCCmdArgs2D& args, MMMatrix& x, MMMatrix& x_ng, MMMatrix& x_nr, double PSNR_ng, double PSNR_nr, int msecs)
{
	ofstream file;
	string matlabOutput = changeExtension(inputFilename, ".m");
	file.open(matlabOutput.c_str());
	if(!file.is_open())
		DISPLAY_ERROR_AND_EXIT("No se pudo crear el archivo de salida: \"" + matlabOutput + "\"");

	string pgmNgOutput = changeExtension(inputFilename, "_ng.pgm");
	string pgmNrOutput = changeExtension(inputFilename, "_nr.pgm");

	// imprimir datos de interes por consola
	PRINT_MSG("Resultados\n------------");
	PRINT_MSG_EXPR("Imagen", inputFilename);
	PRINT_MSG_EXPR("Función generadora de ruido", args.ng_fun);
	PRINT_MSG_EXPR("Nivel de ruido", args.ng_level);
	PRINT_MSG_EXPR("Función de filtrado", args.nr_fun);
	PRINT_MSG_EXPR("Nivel de filtrado", args.nr_level);
	PRINT_MSG_EXPR("Tamaño de bloque", args.block_size);
	PRINT_MSG_EXPR("Imagen con ruido agregado", pgmNgOutput);
	PRINT_MSG_EXPR("Imagen con ruido filtrado", pgmNrOutput);
	PRINT_MSG_EXPR("PSNR de la imagen con ruido", PSNR_ng);
	PRINT_MSG_EXPR("PSNR de la imagen después de filtrar", PSNR_nr);
	PRINT_MSG_EXPR("Tiempo de ejecución (ms)", msecs);
	PRINT_MSG(" ");

	// imprimir datos de interes a un archivo de matlab
	PRINT_MATLAB_STR(file, inputFilename);
	PRINT_MATLAB_STR(file, args.ng_fun);
	PRINT_MATLAB_MAT(file, args.ng_level);
	PRINT_MATLAB_STR(file, args.nr_fun);
	PRINT_MATLAB_MAT(file, args.nr_level);
	PRINT_MATLAB_MAT(file, args.block_size);
	PRINT_MATLAB_STR(file, pgmNgOutput);
	PRINT_MATLAB_STR(file, pgmNrOutput);
	PRINT_MATLAB_MAT(file, PSNR_ng);
	PRINT_MATLAB_MAT(file, PSNR_nr);
	PRINT_MATLAB_MAT(file, msecs);

	file.close();

	writePGM(pgmNgOutput, x_ng);
	writePGM(pgmNrOutput, x_nr);
}