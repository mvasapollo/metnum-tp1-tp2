#ifndef __NOISE_GENERATION__
#define __NOISE_GENERATION__

#include <iostream>
using namespace std;

#include "../lib/metnum-math/metnum-math.h"

#define 	NG_ADDITIVE		"aditivo"
#define 	NG_SPECKLE		"speckle"
#define 	NG_SALTNPEPPER	"saltnpepper"

MMVector callNoiseGeneratingFunction1D(MMVector& x, string ng_function, double ng_level);

MMMatrix callNoiseGeneratingFunction2D(MMMatrix& x, string ng_function, double ng_level);

MMMatrix& normalize(MMMatrix& x);

#endif // __NOISE_GENERATION__