#ifndef __NOISE_REDUCTION__
#define __NOISE_REDUCTION__

#include <iostream>
using namespace std;

#include "../lib/metnum-math/metnum-math.h"

#define 	NR_THRESHOLD		"umbral"
#define 	NR_QUANTILE			"cuantil"
#define 	NR_SPECTRAL			"espectral"

MMVector callNoiseReducingFunction1D(MMVector& y, string nr_function, double nr_level);

MMMatrix callNoiseReducingFunction2D(MMMatrix& y, string nr_function, double nr_level);

#endif // __NOISE_REDUCTION__