#ifndef __MACROS__
#define __MACROS__

#include <cstdlib>		/* exit */

#define		PRINT_MSG(msg)								\
	{ std::cout << (msg) << std::endl; }				\

#define		PRINT_EXPR(expr)												\
	{ std::cout << #expr << " = " << (expr) << ";" << std::endl; }		\

#define		PRINT_EXPR_STREAM(expr,os)										\
	{ (os) << #expr << " = " << (expr) << ";" << std::endl; }			\

#define		PRINT_MSG_EXPR_STREAM(msg,expr,os)								\
	{ (os) << (msg) << " = " << (expr) << ";" << std::endl; }			\

#define		PRINT_MSG_EXPR(msg,expr)										\
	{ std::cout << (msg) << " = " << (expr) << ";" << std::endl; }		\

#define		DISPLAY_ERROR_AND_EXIT(msg)																\
	{																								\
		std::cerr << "Error en la operación \"" << __FUNCTION__ << "\" | " << (msg) << "." << std::endl;	\
		std::exit(-1);																				\
	}																								\

#define		BEGIN_TIMER()		clock_t _start_t_ = clock();
#define		MSECS_ELAPSED()		((int)((clock() - _start_t_)* 1000 / CLOCKS_PER_SEC))

#endif // __MACROS__
