#ifndef __MM_ERROR_H__
#define __MM_ERROR_H__

/* Mensajes de error */

#define		ERROR_MSG_1(i)			("Índice de fila fuera de rango, i: " + int2str(i))
#define		ERROR_MSG_2(j)			("Índice de columna fuera de rango, j: " + int2str(j))
#define		ERROR_MSG_3(i)			("Índice lineal fuera de rango, i: " + int2str(i))
#define		ERROR_MSG_4(i)			("Permutación de filas fuera de rango, i: " + int2str(i))
#define		ERROR_MSG_5(j)			("Permutación de filas fuera de rango, j: " + int2str(j))
#define		ERROR_MSG_6(i)			("Columna fuera de rango, i: " + int2str(i))
#define		ERROR_MSG_7(i,size)		("Bloque inválido, filas fuera de rango, i: " + int2str(i) + ", tamaño:" + int2str(size))
#define		ERROR_MSG_8(j,size)		("Bloque inválido, columnas fuera de rango, j: " + int2str(j) + ", tamaño:" + int2str(size))
#define		ERROR_MSG_9				("Asignación inválida, el rvalue y lvalue no tienen las mismas dimensiones")
#define		ERROR_MSG_10			("Las dimensiones no concuerdan para realizar la suma de matrices")
#define		ERROR_MSG_11			("Las dimensiones no concuerdan para realizar la resta de matrices")
#define		ERROR_MSG_12			("Las dimensiones no concuerdan para realizar el producto de matrices")
#define		ERROR_MSG_13			("Las dimensiones no concuerdan para el producto elemento a elemento")
#define		ERROR_MSG_14(A)			("No se puede factorizar LU, la matriz debe ser cuadrada. Dimensiones: ("+ int2str((A).rows()) + ", " + int2str((A).cols()) + ")")
#define		ERROR_MSG_15(N1,N2)		("Permutación inválida, distinta cantidad de filas ( " + int2str(N1) + " != " + int2str(N2) + " )")
#define 	ERROR_MSG_16(N1,N2)		("No se puede resolver el sistema, las dimensiones son inconsistentes( " + int2str(N1) + " != " + int2str(N2) + " )")
#define		ERROR_MSG_17(f,c)		("Para transponer in situ la matriz debe ser cuadrada, #filas: " + int2str(f) + ", #columnas: " + int2str(c))

#endif // __MM_ERROR_H__
