#include <iomanip>      // std::setw
#include <sstream>
using namespace std;

#include "../macros.h"
#include "MMError.h"
#include "MMAuxiliaries.h"
#include "MMMatrix.h"

/* Implementaciones */

MMMatrix::MMMatrix(unsigned N, unsigned M) :
		N(N), M(M) {
	alloc();

	init_perm();
}

MMMatrix::MMMatrix(unsigned N) :
		N(N), M(N) {
	alloc();

	init_perm();
}

MMMatrix::MMMatrix(unsigned N, double value) :
		N(N), M(1) {
	alloc();

	init_perm();

	for (int i = 0; i < N; ++i)
		(*this)(i) = value;
}

MMMatrix::MMMatrix(const MMMatrix& om) :
		N(om.N), M(om.M) {
	alloc();

	for (int i = 0; i < N; ++i)
		this->rows_perm[i] = om.rows_perm[i];

	MM_MAP_IJ(*this,om(i,j));
}

MMMatrix::~MMMatrix() {
	dealloc();
}

void MMMatrix::alloc() {
	mat = new double[N * M];
	rows_perm = new unsigned[N];
}

void MMMatrix::dealloc() {
	delete[] mat;
	delete[] rows_perm;
}

void MMMatrix::init_perm() {
	for (int i = 0; i < N; ++i)
			rows_perm[i] = ((unsigned) i);
}

unsigned MMMatrix::size() const {
	return MAX(rows(),cols());
}

unsigned MMMatrix::rows() const {
	return N;
}

unsigned MMMatrix::cols() const {
	return M;
}

double MMMatrix::operator()(int i, int j) const {
	if (OUT_OF_RANGE(0,i,N))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_1(i));
	if (OUT_OF_RANGE(0,j,M))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_2(j));

	return mat[rows_perm[i] * M + j];
}

double& MMMatrix::operator()(int i, int j) {
	if (OUT_OF_RANGE(0,i,N))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_1(i));
	if (OUT_OF_RANGE(0,j,M))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_2(j));

	return mat[rows_perm[i] * M + j];
}

double& MMMatrix::operator()(int i) {
	if (OUT_OF_RANGE(0,i,N * M))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_3(i));

	if (cols() == 0)
		return (*this)(i, 0);
	else
		return (*this)(i / cols(), i % cols());
}

double MMMatrix::operator()(int i) const {
	if (OUT_OF_RANGE(0,i,N * M))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_3(i));

	if (cols() == 0)
		return (*this)(i, 0);
	else
		return (*this)(i / cols(), i % cols());
}

MMMatrix MMMatrix::t() {
	MMMatrix m(cols(),rows());
	MM_MAP_IJ(m, (*this)(j,i) );

	return m;
}

void MMMatrix::tInPlace() {
	if( rows() != cols() )
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_17(rows(), cols()));

	for (int i = 0; i < rows(); ++i)
		for (int j = 0; j < i; ++j)
			swap((*this)(i,j),(*this)(j,i));
}

void MMMatrix::permutateR(int i, int j) {
	if (OUT_OF_RANGE(0,i,N))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_4(i));
	if (OUT_OF_RANGE(0,j,N))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_5(j));

	swap(i, j, rows_perm);
}

void MMMatrix::permutateR(const MMMatrix& om) {
	if (rows() != om.rows())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_15(rows(),om.rows()));

	for (int i = 0; i < N; ++i)
		rows_perm[i] = om.rows_perm[i];
}

MMMatrix MMMatrix::view(int ii, int jj, unsigned n, unsigned m) const {

	MMMatrix vw(n,m);
	MM_MAP_IJ(vw,(*this)(ii+i,jj+j));
	return vw;
}

MMMatrix MMMatrix::col(int i) const {
	if (OUT_OF_RANGE(0,i,M))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_6(i));

	return view(0,i,N,1);
}

MMMatrix MMMatrix::block(int i, int j, unsigned size) const {
	if (OUT_OF_RANGE(0,i,N - size + 1))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_7(i,size));
	if (OUT_OF_RANGE(0,j,M - size + 1))
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_8(j,size));

	return view(i,j,size,size);
}

MMMatrix& MMMatrix::operator=(const MMMatrix& rhs) {
	if (N != rhs.rows() || M != rhs.cols())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_9);
	
	(*this).permutateR(rhs);
	MM_MAP_IJ((*this), rhs(i,j) );
	return *this;
}

MMMatrix MMMatrix::operator+(const MMMatrix& rhs) {
	if (rows() != rhs.rows() || cols() != rhs.cols())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_10);
	
	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m, (*this)(i,j) + rhs(i,j) );
	return m;
}

MMMatrix MMMatrix::operator-(const MMMatrix& rhs) {
	if (rows() != rhs.rows() || cols() != rhs.cols())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_11);
	
	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m, (*this)(i,j) - rhs(i,j) );
	return m;
}

MMMatrix MMMatrix::operator*(const MMMatrix& rhs) {
	if ( cols() != rhs.rows() )
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_12);
	
	MMMatrix m(rows(), rhs.cols());
	MM_WALK_IJ(m,
		double dot = 0;
		for(int k = 0; k < cols(); k++)
			dot += ((*this)(i,k) * rhs(k,j));
		m(i,j) = dot;
	);
	
	return m;
}

MMMatrix MMMatrix::operator&(const MMMatrix& rhs) {
	if (rows() != rhs.rows() || cols() != rhs.cols())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_13);

	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m, (*this)(i,j) * rhs(i,j) );
	return m;
}

MMMatrix MMMatrix::operator+(double rhs)
{
	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m,(*this)(i,j) + rhs);
	return m;
}

MMMatrix MMMatrix::operator-(double rhs)
{
	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m,(*this)(i,j) - rhs);
	return m;
}

MMMatrix MMMatrix::operator*(double rhs)
{
	MMMatrix m(rows(), cols());
	MM_MAP_IJ(m,(*this)(i,j) * rhs);
	return m;
}

std::ostream &operator<<(std::ostream &os, MMMatrix m) {
	os.precision(7);
	os << "[" << endl;
	for (int i = 0; i < m.rows(); ++i) {
		os << "\t[";
		for (int j = 0; j < m.cols(); ++j) {
			os << left << std::setw(15) << m(i, j) << " ";
		}
		os << "];" << std::endl;
	}
	os << "]";

	return os;
}