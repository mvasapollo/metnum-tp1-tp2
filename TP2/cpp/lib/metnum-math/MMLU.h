#ifndef __MM_LU_H__
#define __MM_LU_H__

#include <iostream>
#include <vector>

#include "MMMatrix.h"
#include "MMMatrixUtils.h"

class MMLU {
public:
	MMLU(const MMMatrix& A);
	~MMLU();

	MMMatrix solve(const MMMatrix& b);
	
	unsigned N;
	MMMatrix L;
	MMMatrix U;

	friend std::ostream &operator<<(std::ostream &os, MMLU lu);

private:
	int findPivotRow(int k);
	void LUdecomposition();
	MMMatrix backwardSubstitution(const MMMatrix& y);
	MMMatrix forwardSubstitution(const MMMatrix& b);
};

#endif // __MM_LU_H__
