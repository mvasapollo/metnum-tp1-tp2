#include "MMAuxiliaries.h"

#include <sstream>
#include <iostream>
using namespace std;

void swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

void swap(unsigned& a, unsigned& b)
{
	unsigned temp = a;
	a = b;
	b = temp;
}

void swap(unsigned*& a, unsigned*& b)
{
	unsigned* temp = a;
	a = b;
	b = temp;
}

void swap(int i, int j, unsigned* v)
{
	unsigned temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

string int2str(int x)
{
	stringstream ss;
	ss << x;
	return ss.str();
}