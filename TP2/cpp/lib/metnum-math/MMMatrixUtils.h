#ifndef __MM_MATRIX_UTILITIES__
#define __MM_MATRIX_UTILITIES__

#include "MMMatrix.h"
#include "MMAuxiliaries.h"

class MMMatrixUtils
{
public:
	static MMMatrix identity(unsigned N);
	static MMMatrix zeros(unsigned N);
	static MMMatrix linvector(unsigned N, double min, double max);
	static MMMatrix rand(unsigned N, unsigned M);
	static MMMatrix cos(const MMMatrix& om);
	static MMMatrix abs(const MMMatrix& om);
	static MMMatrix sqrt(const MMMatrix& om);
	static MMMatrix square(const MMMatrix& om);
	static MMMatrix round(const MMMatrix& om);
	static MMMatrix permutationMatrix(const MMMatrix& om);
	static double max(const MMMatrix& om);
	static void minmax(const MMMatrix& om, double& min, double& max);
	static double range(const MMMatrix& om);
	static double sum(const MMMatrix& om);
	static void sortElementsByAbs(MMMatrix& om);
	
private:
};

#endif // __MM_MATRIX_UTILITIES__
