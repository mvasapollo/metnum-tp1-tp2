#ifndef __MM_AUXILIARIES_H__
#define __MM_AUXILIARIES_H__

#include <iostream>

#define		WITHIN_RANGE(low,val,max)		(((low)<=(val)) && ((val)<(max)))
#define		OUT_OF_RANGE(low,val,max)		(!WITHIN_RANGE(low,val,max))
#define		MIN(a,b)						(((a)<(b))?(a):(b))
#define		MAX(a,b)						(((a)>(b))?(a):(b))
#define		ROUND(dbl)						((int)(dbl+ 0.5))

void swap(int& a, int& b);
void swap(unsigned& a, unsigned& b);
void swap(unsigned*& a, unsigned*& b);
void swap(int i, int j, unsigned* v);
std::string int2str(int x);

#endif // __MM_AUXILIARIES_H__
