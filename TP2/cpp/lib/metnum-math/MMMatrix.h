#ifndef __MM_MATRIX_H__
#define __MM_MATRIX_H__

#include <vector>
using namespace std;

class MMMatrix {
public:
	MMMatrix(unsigned N, unsigned M);
	MMMatrix(unsigned N);
	MMMatrix(unsigned N, double value);
	MMMatrix(const MMMatrix& om);
	~MMMatrix();

	unsigned size() const;
	unsigned rows() const;
	unsigned cols() const;

	double& operator()(int i, int j);
	double operator()(int i, int j) const;
	double& operator()(int i);
	double operator()(int i) const;

	MMMatrix t();
	void tInPlace();

	void permutateR(int i, int j);
	void permutateR(const MMMatrix& om);

	MMMatrix col(int i) const;
	MMMatrix block(int i, int j, unsigned size) const;

	MMMatrix& operator=(const MMMatrix& rhs);
	MMMatrix operator+(const MMMatrix& rhs);
	MMMatrix operator-(const MMMatrix& rhs);
	MMMatrix operator*(const MMMatrix& rhs);
	MMMatrix operator&(const MMMatrix& rhs);	/* producto componente a componente */

	MMMatrix operator+(double rhs);
	MMMatrix operator-(double rhs);
	MMMatrix operator*(double rhs);

	friend std::ostream &operator<<(std::ostream &os, MMMatrix m);

private:
	void alloc();
	void dealloc();
	void init_perm();
	MMMatrix view(int i, int j, unsigned n, unsigned m) const;

	unsigned N, M;
	double* mat;
	unsigned *rows_perm;
};

/* Auxiliares */

#define		MM_MAP_I(vec,expr)								\
{															\
	for (int i = 0; i < (vec).size(); ++i)					\
			{((vec)(i)) = (expr);}							\
}															
															

#define		MM_MAP_IJ(mat,expr)								\
{															\
	for (int i = 0; i < (mat).rows(); ++i)					\
		for (int j = 0; j < (mat).cols(); ++j)				\
			{((mat)(i,j)) = (expr);}						\
}															
															

#define		MM_WALK_I(vec,stmt)								\
{															\
	for (int i = 0; i < (vec).size(); ++i)					\
			{stmt;}											\
}
															

#define		MM_WALK_IJ(mat,stmt)							\
{															\
	for (int i = 0; i < (mat).rows(); ++i)					\
		for (int j = 0; j < (mat).cols(); ++j)				\
			{stmt;}											\
}

															
															
#endif // __MM_MATRIX_H__
