#include <iomanip>	/* setw */
#include <cmath>		/* cos */
#include <iostream>
using namespace std;

#include "../macros.h"
#include "MMError.h"
#include "MMAuxiliaries.h"
#include "MMMatrix.h"
#include "MMMatrixUtils.h"
#include "MMLU.h"

#define		MMLU_TOLERANCE		(1e-8)

MMLU::MMLU(const MMMatrix& A) : N(A.size()), U(A), L(MMMatrixUtils::zeros(A.size())) {
	if (A.rows() != A.cols())
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_14(A));

	LUdecomposition();
}

MMLU::~MMLU() {
}

void MMLU::LUdecomposition() {

	for (int k = 0; k < N - 1; ++k) {

		int pivot = k;
		int max = abs(U(k,k));
		for (int i = k+1; i < N; ++i)
		{
			double value = abs(U(i,k));
			if( max < value )
			{
				pivot = i;
				max = value;
			}
		}

		if( max < MMLU_TOLERANCE )
			continue;

		L.permutateR(k,pivot);
		U.permutateR(k,pivot);

		for (int i = k + 1; i < N; ++i) {
			
			double mult = U(i, k) / U(k, k);

			for (int j = k; j < N; ++j)
				U(i, j) -= mult * U(k, j);

			L(i, k) = mult;
		}
	}

	L = L + MMMatrixUtils::identity(N);
}

std::ostream &operator<<(std::ostream &os, MMLU lu) {
	PRINT_MSG_EXPR_STREAM("L", lu.L, os);
	PRINT_MSG_EXPR_STREAM("U", lu.U, os);
	PRINT_MSG_EXPR_STREAM("P", MMMatrixUtils::permutationMatrix(lu.U), os);

	return os;
}

MMMatrix MMLU::solve(const MMMatrix& b) {

	if( N != b.rows() )
		DISPLAY_ERROR_AND_EXIT(ERROR_MSG_16(N,b.rows()));

	MMMatrix x(b.rows(),b.cols());
	for (int j = 0; j < b.cols(); ++j)
	{
		MMMatrix b_col = b.col(j);
		b_col.permutateR(U);
		MMMatrix y = forwardSubstitution(b_col);
		MMMatrix x_col = backwardSubstitution(y);
		for (int i = 0; i < N; ++i)
			x(i,j) = x_col(i,0);
	}

	return x;
}

MMMatrix MMLU::forwardSubstitution(const MMMatrix& b) {
	MMMatrix y(N,0.0);

	for (int i = 0; i < N; i++) {
		y(i) = b(i);
		for (int j = 0; j < i; j++) {
			y(i) -= L(i, j) * y(j);
		}
		y(i) /= L(i, i);
	}
	return y;
}

MMMatrix MMLU::backwardSubstitution(const MMMatrix& y) {
	MMMatrix x(N,0.0);

	for (int i = N - 1; i >= 0; i--) {
		x(i) = y(i);
		for (int j = i + 1; j < N; j++) {
			x(i) -= U(i, j) * x(j);
		}
		x(i) /= U(i, i);
	}
	return x;
}