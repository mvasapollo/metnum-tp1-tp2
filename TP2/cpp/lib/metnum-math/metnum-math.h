#ifndef __METNUM_MATH__
#define __METNUM_MATH__

#define 	cwiseCos(mat)		(((mat).array().cos()).matrix())

#include "MMAuxiliaries.h"
#include "MMMatrix.h"
#include "MMMatrixUtils.h"
#include "MMLU.h"

typedef MMMatrix MMVector;

#endif // __METNUM_MATH__
