close all
addpath noise-generation
addpath noise-reduction

%%%%% Parámetros

ng_fun = @ng_additive							% funcion generadora de ruido
ng_level = 0.3									% nivel de ruido
nr_fun = @nr_threshold							% funcion reductora de ruido
nr_value = 0.02 								% argumento de nr_fun
block_size = 32 									% solo influye si es > 0,
												% y divide al tamaño de la imagen

%%%%% Generar datos [en el programa de C++ se van a cargar por archivo]

% cargo la imagen
im_filepath = '../data/images/imtexmos3s512.pgm'		% path a la imagen
im = imread(im_filepath);							

% genero matriz de datos
x = double(im);
N = size(x,1);										% tamaño de la muestra


%%%%% Procesar datos

% genero datos ruidosos a partir de los originales
x_ng = ng_fun(x, ng_level);

% realizo reducción de ruido
x_nr = DCT2D(x_ng, nr_fun, nr_value, block_size);

% computo el PSNR de los datos con ruido y de los datos después de 
% reducir ruido, siempre respecto a los originales
PSNR_NG = computePSNR(x, x_ng);
PSNR_NR = computePSNR(x, x_nr);


%%%%% Mostrar resultados [en el programa de C++ se van a exportar a un archivo]

% imprimo el PSNR por consola
disp(['PSNR (señal con ruido agregado): ' num2str(PSNR_NG)]);
disp(['PSNR (después de reducir ruido): ' num2str(PSNR_NR)]);
disp(' ');

% muestro la imagen original, la imagen con ruido, y la
% imagen después de la reducción de ruido
figure;
subplot(1,3,1);imshow(uint8(x)); title('Imagen original');
subplot(1,3,2); imshow(uint8(x_ng)); title(['Imagen con ruido agregado. (PSNR ' num2str(PSNR_NG,'%.2f') ')']);
subplot(1,3,3); imshow(uint8(x_nr)); title(['Imagen después de reducir ruido. (PSNR ' num2str(PSNR_NR,'%.2f') ')']);
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana