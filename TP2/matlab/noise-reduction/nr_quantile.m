function y_nr = nr_quantile(y, nr_value)
	% anula el porcentaje 'nr_value'*100% de frecuencias de menor magnitud
	
	y_nr = y;
	quant = quantile(abs(y_nr(:)), nr_value);
	y_nr(abs(y_nr) < quant) = 0;
end