function y_nr = nr_threshold(y, nr_value)
	% anula todas las frecuencias con magnitud menor a max(y) * 'nr_value'
	
	y_nr = y;
	nr_value = max(max(abs(y_nr))) * nr_value;
	y_nr(abs(y_nr) < nr_value) = 0;
end