close all

psnrs_aditivo

figure
hold on;
plot(nivelfs_aditivo_umbral_tipo1,PSNRs_aditivo_umbral_tipo1,'rx-');
plot(nivelfs_aditivo_umbral_tipo2,PSNRs_aditivo_umbral_tipo2,'gx-');
plot(nivelfs_aditivo_umbral_tipo3,PSNRs_aditivo_umbral_tipo3,'bx-');
title('PSNR vs nivelf (ruido aditivo, filtrado umbral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/aditivo_umbral.pdf');

figure
hold on;
plot(nivelfs_aditivo_cuantil_tipo1,PSNRs_aditivo_cuantil_tipo1,'rx-');
plot(nivelfs_aditivo_cuantil_tipo2,PSNRs_aditivo_cuantil_tipo2,'gx-');
plot(nivelfs_aditivo_cuantil_tipo3,PSNRs_aditivo_cuantil_tipo3,'bx-');
title('PSNR vs nivelf (ruido aditivo, filtrado cuantil)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/aditivo_cuantil.pdf');

figure
hold on;
plot(nivelfs_aditivo_espectral_tipo1,PSNRs_aditivo_espectral_tipo1,'rx-');
plot(nivelfs_aditivo_espectral_tipo2,PSNRs_aditivo_espectral_tipo2,'gx-');
plot(nivelfs_aditivo_espectral_tipo3,PSNRs_aditivo_espectral_tipo3,'bx-');
title('PSNR vs nivelf (ruido aditivo, filtrado espectral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/aditivo_espectral.pdf');

psnrs_speckle

figure
hold on;
plot(nivelfs_speckle_umbral_tipo1,PSNRs_speckle_umbral_tipo1,'rx-');
plot(nivelfs_speckle_umbral_tipo2,PSNRs_speckle_umbral_tipo2,'gx-');
plot(nivelfs_speckle_umbral_tipo3,PSNRs_speckle_umbral_tipo3,'bx-');
title('PSNR vs nivelf (ruido speckle, filtrado umbral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/speckle_umbral.pdf');

figure
hold on;
plot(nivelfs_speckle_cuantil_tipo1,PSNRs_speckle_cuantil_tipo1,'rx-');
plot(nivelfs_speckle_cuantil_tipo2,PSNRs_speckle_cuantil_tipo2,'gx-');
plot(nivelfs_speckle_cuantil_tipo3,PSNRs_speckle_cuantil_tipo3,'bx-');
title('PSNR vs nivelf (ruido speckle, filtrado cuantil)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/speckle_cuantil.pdf');

figure
hold on;
plot(nivelfs_speckle_espectral_tipo1,PSNRs_speckle_espectral_tipo1,'rx-');
plot(nivelfs_speckle_espectral_tipo2,PSNRs_speckle_espectral_tipo2,'gx-');
plot(nivelfs_speckle_espectral_tipo3,PSNRs_speckle_espectral_tipo3,'bx-');
title('PSNR vs nivelf (ruido speckle, filtrado espectral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/speckle_espectral.pdf');

psnrs_saltnpepper

figure
hold on;
plot(nivelfs_saltnpepper_umbral_tipo1,PSNRs_saltnpepper_umbral_tipo1,'rx-');
plot(nivelfs_saltnpepper_umbral_tipo2,PSNRs_saltnpepper_umbral_tipo2,'gx-');
plot(nivelfs_saltnpepper_umbral_tipo3,PSNRs_saltnpepper_umbral_tipo3,'bx-');
title('PSNR vs nivelf (ruido salt and pepper, filtrado umbral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/saltnpepper_umbral.pdf');

figure
hold on;
plot(nivelfs_saltnpepper_cuantil_tipo1,PSNRs_saltnpepper_cuantil_tipo1,'rx-');
plot(nivelfs_saltnpepper_cuantil_tipo2,PSNRs_saltnpepper_cuantil_tipo2,'gx-');
plot(nivelfs_saltnpepper_cuantil_tipo3,PSNRs_saltnpepper_cuantil_tipo3,'bx-');
title('PSNR vs nivelf (ruido salt and pepper, filtrado cuantil)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/saltnpepper_cuantil.pdf');

figure
hold on;
plot(nivelfs_saltnpepper_espectral_tipo1,PSNRs_saltnpepper_espectral_tipo1,'rx-');
plot(nivelfs_saltnpepper_espectral_tipo2,PSNRs_saltnpepper_espectral_tipo2,'gx-');
plot(nivelfs_saltnpepper_espectral_tipo3,PSNRs_saltnpepper_espectral_tipo3,'bx-');
title('PSNR vs nivelf (ruido salt and pepper, filtrado espectral)');
xlabel('k (s/u)');
ylabel('PSNR (dB)');
legend({'Clase 1','Clase 2','Clase 3'});
export_fig('../informe/graficos/psnrs/saltnpepper_espectral.pdf');