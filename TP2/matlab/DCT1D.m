function [y y_nr x_nr] = DCT1D(x, nr_fun, nr_fun_arg)

	% tamaño de la muestra
	N = numel(x);

	% obtengo la matriz de transformación 'M' y su fact L*U = P*M
	M = matrizDCT(N);
	[M_L M_U M_P] = lu(M);

	% obtengo la transformada y
	y = M * x;
	
	% aplico la funcion reductora de ruido a 'y'
	y_nr = nr_fun(y, nr_fun_arg);
	
	% recupero la muestra con ruido reducido
	x_nr = restoreData(M_L, M_U, M_P, y_nr);

end

function x = restoreData(M_L, M_U, M_P, y)
	
	% resuelvo el sistema y_nr = M * x_nr
	x = solveLSE1D(M_L, M_U, M_P, y);

end