function x = forwardSubstitution(L,y)
	n = length( y );
	x = zeros( n, 1 );
	for i=1:n
	   x(i) = y(i) - L(i, :)*x;
	end
end