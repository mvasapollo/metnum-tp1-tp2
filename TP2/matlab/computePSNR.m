function PSNR = computePSNR(x, x_app)
	ECM = mean(mean((x - x_app).^2));
	PSNR = 10 * log10((range(x(:)))^2 / ECM);
end