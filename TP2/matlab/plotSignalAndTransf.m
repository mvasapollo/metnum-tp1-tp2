close all

addpath export_fig

t = linspace(0,5,500);
x = -3*cos(2 * t) + 1*cos(4* t) + 2 * cos(7 * t);
M = matrizDCT(length(x));
y = M * x';

figure;
subplot(2,1,1); plot(t,x); title('x(t): -3*cos(2 * t) + 1*cos(4* t) + 2 * cos(7 * t)'); xlabel('t'); ylabel('x(t)');
subplot(2,1,2); plot(0:(length(x)/10),y(1:((length(x)/10)+1))); title('Transformada'); xlabel('f'); ylabel('magnitud');
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
export_fig(['../informe/graficos/sen_tranf_tipo1' '.pdf']);

t = linspace(0,5,1000);
x = cos(t.^2);
M = matrizDCT(length(x));
y = M * x';

figure;
subplot(2,1,1); plot(t,x); title('x(t): cos(t.^2)'); xlabel('t'); ylabel('x(t)');
subplot(2,1,2); plot(0:(length(x)/10),y(1:((length(x)/10)+1))); title('Transformada'); xlabel('f'); ylabel('magnitud');
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
export_fig(['../informe/graficos/sen_tranf_tipo2' '.pdf']);

t = linspace(0,5,1500);
x = -sign(t - 2.5) - 2 * sign(t - 4) + t - 3;
M = matrizDCT(length(x));
y = M * x';

figure;
subplot(2,1,1); plot(t,x); title('x(t): -sign(t - 2.5) - 2 * sign(t - 4) + t - 3'); xlabel('t'); ylabel('x(t)');
subplot(2,1,2); plot(0:(length(x)/8),y(1:((length(x)/8)+1))); title('Transformada'); xlabel('f'); ylabel('magnitud');
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
export_fig(['../informe/graficos/sen_tranf_tipo3' '.pdf']);