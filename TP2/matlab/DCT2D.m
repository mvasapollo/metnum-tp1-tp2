function x_nr = DCT2D(x, nr_fun, nr_fun_arg, block_size)

	% tamaño de la muestra
	N = size(x,1);

	if( block_size == 0 )
		block_size = N;
	elseif( mod(N,block_size) ~= 0 )
		error(['No se puede dividir la imagen en bloques de iguales dimensiones.' ...
			' El tamaño de bloque ingresado es ' num2str(block_size) ' y el tamaño' ...
			' de la imagen es ' num2str(N) '.']);
	end

	% obtengo la matriz de transformación 'M' para el tamaño de un bloque, y su fact L*U = P*M
	M = matrizDCT(block_size); M_t = M';
	[M_L M_U M_P] = lu(M);

	% proceso cada bloque por separado
	x_nr = zeros(size(x));
	for ii=1:block_size:N
		for jj=1:block_size:N

			% obtengo el bloque
			bl_ii = ii:(ii+block_size-1);
			bl_jj = jj:(jj+block_size-1);
			bl_x = x( bl_ii, bl_jj );
			
			% obtengo la transformada 'y' del bloque
			y = M * bl_x * M_t;
			
			% aplico la funcion reductora de ruido a 'y'
			y_nr = nr_fun(y, nr_fun_arg);
			
			% recupero y almaceno el bloque con ruido reducido
			x_nr(bl_ii, bl_jj) = restoreData(M_L, M_U, M_P, y_nr);
		end
	end
end

function x = restoreData(M_L, M_U, M_P, y)
	% resuelve el sistema y_nr = M * x * M' en dos pasos
	% 	1) y = M * z
	% 	2) z = x * M' (o equivalentemente, M * x' = z')

	z = solveLSE2D(M_L, M_U, M_P, y);

	x = solveLSE2D(M_L, M_U, M_P, z')';

end
