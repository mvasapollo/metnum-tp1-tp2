function x_ng = ng_speckle(x, ng_level)
	% multiplica cada componente por un factor uniformemente distribuido
	% entre [1 - ng_rate/2 * rango(x), 1 + ng_rate/2 * rango(x)]

	factors = ones(size(x)) + 0.5 * log10(ng_level * 0.5 * range(x(:))) * (2 * rand(size(x)) - 1);
	x_ng = factors .* x;
end