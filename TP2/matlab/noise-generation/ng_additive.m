function x_ng = ng_additive(x, ng_level)
	% suma a cada componente un valor aleatorio uniformemente distribuido
	% entre [- ng_rate/2 * rango(x), ng_rate/2 * rango(x)]

	ng = (ng_level * 0.5 * range(x(:))) * (2 * rand(size(x)) - ones(size(x)));
	x_ng = x + ng;
end