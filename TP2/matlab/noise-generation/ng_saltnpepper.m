function x_ng = ng_saltnpepper(x, ng_level)
	% altera el 'ng_level'*100 % de la muestra, transformando la mitad
	% de los puntos distorsionados al menor valor encontrado en la muestra,
	% y el resto al mayor

	x_ng = x;
	whitePts = (rand(numel(x),1) < (ng_level/2));
	x_ng( whitePts ) = max(abs(x(:)));
	darkPts = (rand(numel(x),1) < (ng_level/2));
	x_ng( darkPts ) = min(abs(x(:)));
end