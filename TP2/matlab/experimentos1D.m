close all
addpath noise-generation
addpath noise-reduction

%%%%% Parámetros

ng_fun = @ng_additive							% funcion generadora de ruido
ng_level = 0.3									% nivel de ruido
nr_fun = @nr_threshold							% funcion reductora de ruido
nr_value = 0.1									% argumento de nr_fun


%%%%% Generar datos [en el programa de C++ se van a cargar por archivo]

% defino la señal
N = 1000
range = [0 5]                                       % dominio de la señal
signal = @(t) (2 * cos(2*t) - 5 * sin(t) + 10)      % funcion que define la señal

% genero vector de datos
t = linspace(range(1),range(2),N)';
x = arrayfun(signal, t);


%%%%% Procesar datos

% genero datos ruidosos a partir de los originales
x_ng = ng_fun(x, ng_level);

% realizo reducción de ruido
[y y_nr x_nr] = DCT1D(x_ng, nr_fun, nr_value);

% computo el PSNR de los datos con ruido y de los datos después de 
% reducir ruido, siempre respecto a los originales
PSNR_NG = computePSNR(x, x_ng);
PSNR_NR = computePSNR(x, x_nr);


%%%%% Mostrar resultados [en el programa de C++ se van a exportar a un archivo]

% imprimo el PSNR por consola
disp(['PSNR (señal con ruido agregado): ' num2str(PSNR_NG)]);
disp(['PSNR (después de reducir ruido): ' num2str(PSNR_NR)]);
disp(' ');

% grafico la señal original, la señal con ruido agregado,
% y la señal posterior a la reduccion de ruido
figure;
subplot(3,1,1); plot(t, x); title('Señal original'); xlabel('t'); ylabel('x(t)');
subplot(3,1,2); plot(t,x_ng); title(['Señal con ruido agregado. (PSNR ' num2str(PSNR_NG,'%.2f') ')']); xlabel('t'); ylabel('x(t)');
subplot(3,1,3); plot(t,x_nr); title(['Señal después de reducir ruido. (PSNR ' num2str(PSNR_NR,'%.2f') ')']); xlabel('t'); ylabel('x(t)');
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana

% grafico la transformada de la señal antes y después de la reducción de ruido
figure;
subplot(2,1,1); plot(0:(N-1),y); title('Transformada de la señal con ruido'); xlabel('f'); ylabel('magnitud');
subplot(2,1,2); plot(0:(N-1),y_nr); title('Transformada de la señal después de reducir ruido'); xlabel('f'); ylabel('magnitud');
set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
