function x = solveLSE1D(L, U , P, y)
	% resuelve el sistema A x = y, donde L * U = P * A
	% con x, y vectores
	
	Py = P * y;
	z = forwardSubstitution(L, Py);
	x = backwardSubstitution(U, z);

end
