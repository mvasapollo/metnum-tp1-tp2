function showResults1D(filename)

	addpath export_fig

	eval(['run ' filename]);

	% nombre de archivo
	[pathstr, name, ext] = fileparts(inputFilename);
	ng_level_str = strrep(num2str(args.ng_level),'.','_');
	nr_level_str = strrep(num2str(args.nr_level),'.','_');

	%%%%% Mostrar resultados

	% parámetros
	disp(['ng_fun: ' args.ng_fun])
	disp(['ng_level: ' num2str(args.ng_level)])
	disp(['nr_fun: ' args.nr_fun])
	disp(['nr_level: ' num2str(args.nr_level)])
	disp(['N: ' num2str(size(x,1))])


	% imprimo el PSNR por consola
	disp(['PSNR (señal con ruido agregado): ' num2str(PSNR_ng)]);
	disp(['PSNR (después de reducir ruido): ' num2str(PSNR_nr)]);
	disp(' ');

	% grafico la transformada de la señal antes y después de la reducción de ruido
	close all
	figure;
	subplot(2,1,1); plot(0:(numel(y)-1),y); title(['Transformada de la x(t) con ruido (' args.ng_fun ', ' num2str(args.ng_level) ')']); xlabel('f'); ylabel('magnitud');
	subplot(2,1,2); plot(0:(numel(y_nr)-1),y_nr); title(['Transformada de la x(t) despues de reducir ruido (' args.nr_fun ', ' num2str(args.nr_level) ')']); xlabel('f'); ylabel('magnitud');
	set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
	export_fig(['../informe/graficos/senalAntesDespues/y_' name '_' args.ng_fun '_' ng_level_str '_' args.nr_fun '_' nr_level_str '.pdf']);

	% grafico la señal original, la señal con ruido agregado,
	% y la señal posterior a la reduccion de ruido
	figure;
	subplot(3,1,1); plot(x); title(['x(t) original (N ' num2str(size(x,1)) ')']); xlabel('t'); ylabel('x(t)');
	subplot(3,1,2); plot(x_ng); title(['x(t) con ruido agregado. (' args.ng_fun ', ' num2str(args.ng_level) ', PSNR ' num2str(PSNR_ng,'%.2f') ')']); xlabel('t'); ylabel('x(t)');
	subplot(3,1,3); plot(x_nr); title(['x(t) despues de reducir ruido. (' args.nr_fun ', ' num2str(args.nr_level) ', PSNR ' num2str(PSNR_nr,'%.2f') ')']); xlabel('t'); ylabel('x(t)');
	set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
	export_fig(['../informe/graficos/senalAntesDespues/x_' name '_' args.ng_fun '_' ng_level_str '_' args.nr_fun '_' nr_level_str '.pdf']);

	clear all
end