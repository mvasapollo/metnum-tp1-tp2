function x = solveLSE2D(L, U , P, y)
	% resuelve el sistema A x = y, donde L * U = P * A
	% con x, y matrices de N por N

	N = size(L,1);
	x = zeros(size(y));
	for ii=1:N
		% resuelvo A * col_ii(x) = col_ii(y)
		x(:,ii) = solveLSE1D(L, U, P, y(:,ii));
	end

end