% Generado con los siguientes parámetros:
% $tiposDeSenal = array(1,2,3);

% $tiposDeRuido = array('aditivo');

% $nivelrs = array(0.08);

% $tiposDeFiltrado = array('umbral', 'cuantil', 'espectral');

% $nivelfs = array(
% 	'umbral' => range(0.01,0.15,0.01),
% 	'cuantil' => range(0.92,0.99,0.005),
% 	'espectral' => range(0.62,0.78,0.01),
% );

nivelfs_aditivo_umbral_tipo1 = [0.01;
0.02;
0.03;
0.04;
0.05;
0.06;
0.07;
0.08;
0.09;
0.1;
0.11;
0.12;
0.13;
0.14;
0.15;
];
PSNRs_aditivo_umbral_tipo1 = [22.5275;
24.0966;
26.3846;
29.9538;
33.957;
33.957;
33.6214;
33.6214;
31.1139;
27.6608;
26.5926;
26.5926;
25.4602;
24.183;
24.183;
];
nivelfs_aditivo_cuantil_tipo1 = [0.92;
0.925;
0.93;
0.935;
0.94;
0.945;
0.95;
0.955;
0.96;
0.965;
0.97;
0.975;
0.98;
0.985;
0.99;
];
PSNRs_aditivo_cuantil_tipo1 = [26.6599;
26.9502;
27.164;
27.4782;
27.7612;
28.2419;
28.682;
29.3563;
29.9538;
30.7288;
31.9294;
33.6214;
28.7276;
25.4602;
23.0001;
];
nivelfs_aditivo_espectral_tipo1 = [0.5;
0.515;
0.53;
0.545;
0.56;
0.575;
0.59;
0.605;
0.62;
0.635;
0.65;
0.665;
0.68;
0.695;
0.71;
0.725;
0.74;
0.755;
0.77;
0.785;
0.8;
];
PSNRs_aditivo_espectral_tipo1 = [24.7242;
25.0217;
25.4733;
25.7838;
26.1111;
26.4408;
26.7019;
27.1313;
27.6011;
28.0039;
28.5362;
29.0873;
29.8228;
31.2995;
32.7841;
30.5503;
30.5503;
30.5503;
30.5503;
30.5503;
16.2553;
];
nivelfs_aditivo_umbral_tipo2 = [0.01;
0.02;
0.03;
0.04;
0.05;
0.06;
0.07;
0.08;
0.09;
0.1;
0.11;
0.12;
0.13;
0.14;
0.15;
];
PSNRs_aditivo_umbral_tipo2 = [22.2311;
22.9012;
24.3007;
26.3196;
27.8029;
28.4204;
29.3394;
29.1323;
29.1323;
29.5627;
29.5627;
29.5627;
28.9583;
27.9342;
27.9342;
];
nivelfs_aditivo_cuantil_tipo2 = [0.92;
0.925;
0.93;
0.935;
0.94;
0.945;
0.95;
0.955;
0.96;
0.965;
0.97;
0.975;
0.98;
0.985;
0.99;
];
PSNRs_aditivo_cuantil_tipo2 = [25.9104;
26.0919;
26.2688;
26.4698;
26.6815;
26.8919;
27.1309;
27.3916;
27.6566;
27.9124;
28.1068;
28.7973;
29.1323;
25.0841;
16.0042;
];
nivelfs_aditivo_espectral_tipo2 = [0.5;
0.515;
0.53;
0.545;
0.56;
0.575;
0.59;
0.605;
0.62;
0.635;
0.65;
0.665;
0.68;
0.695;
0.71;
0.725;
0.74;
0.755;
0.77;
0.785;
0.8;
];
PSNRs_aditivo_espectral_tipo2 = [25.4409;
25.7809;
26.0755;
26.4783;
26.7874;
27.2254;
27.7575;
28.381;
29.3901;
29.5966;
26.1479;
23.3431;
20.1022;
20.1022;
17.0652;
17.0652;
14.9691;
14.2203;
14.2203;
12.781;
12.2943;
];
nivelfs_aditivo_umbral_tipo3 = [0.01;
0.02;
0.03;
0.04;
0.05;
0.06;
0.07;
0.08;
0.09;
0.1;
0.11;
0.12;
0.13;
0.14;
0.15;
];
PSNRs_aditivo_umbral_tipo3 = [22.4495;
24.1544;
25.5874;
25.647;
24.7118;
23.7858;
23.5292;
23.0843;
22.5757;
21.8962;
21.7155;
21.368;
20.7109;
20.4011;
20.4011;
];
nivelfs_aditivo_cuantil_tipo3 = [0.92;
0.925;
0.93;
0.935;
0.94;
0.945;
0.95;
0.955;
0.96;
0.965;
0.97;
0.975;
0.98;
0.985;
0.99;
];
PSNRs_aditivo_cuantil_tipo3 = [25.0158;
25.0499;
25.1232;
25.2083;
25.3192;
25.5486;
25.5874;
25.699;
25.7748;
25.7321;
25.5483;
24.9338;
24.5332;
23.5292;
21.9637;
];
nivelfs_aditivo_espectral_tipo3 = [0.5;
0.515;
0.53;
0.545;
0.56;
0.575;
0.59;
0.605;
0.62;
0.635;
0.65;
0.665;
0.68;
0.695;
0.71;
0.725;
0.74;
0.755;
0.77;
0.785;
0.8;
];
PSNRs_aditivo_espectral_tipo3 = [23.8997;
24.0327;
24.2866;
24.5329;
24.7707;
24.977;
25.1462;
25.3199;
25.4659;
25.5619;
25.6272;
25.7306;
25.5069;
25.3388;
25.205;
25.06;
24.575;
24.0739;
23.5352;
22.7107;
21.8403;
];
