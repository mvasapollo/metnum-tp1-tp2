function showResults2D(filename)

	addpath export_fig

	eval(['run ' filename]);

	% nombre de archivo
	[pathstr, name, ext] = fileparts(inputFilename);
	ng_level_str = strrep(num2str(args.ng_level),'.','_');
	nr_level_str = strrep(num2str(args.nr_level),'.','_');

	%%%%% Mostrar resultados

	% cargo las fotos
	im = imread(inputFilename);
	im_ng = imread(pgmNgOutput);
	im_nr = imread(pgmNrOutput);

	% parámetros
	disp(['ng_fun: ' args.ng_fun])
	disp(['ng_level: ' num2str(args.ng_level)])
	disp(['nr_fun: ' args.nr_fun])
	disp(['nr_level: ' num2str(args.nr_level)])
	disp(['block_size: ' num2str(args.block_size)])
	disp(['N: ' num2str(size(im,1))])

	% imprimo el PSNR por consola
	disp(['PSNR (señal con ruido agregado): ' num2str(PSNR_ng)]);
	disp(['PSNR (después de reducir ruido): ' num2str(PSNR_nr)]);
	disp(' ');

	% muestro la imagen original, la imagen con ruido, y la
	% imagen después de la reducción de ruido
	close all
	figure;
	subplot(1,3,1);imshow(im); title(['Imagen original (N ' num2str(size(im,1)) ')']);
	subplot(1,3,2); imshow(im_ng); title(['Imagen con ruido agregado. (' args.ng_fun ', ' num2str(args.ng_level) ', PSNR ' num2str(PSNR_ng,'%.2f') ')']);
	subplot(1,3,3); imshow(im_nr); title(['Imagen despues de reducir ruido. (' args.nr_fun ', ' num2str(args.nr_level) ', PSNR ' num2str(PSNR_nr,'%.2f') ')']);
	set(gcf, 'Position', get(0,'Screensize')); 	% maximizar ventana
	export_fig(['../informe/graficos/imagenAntesDespues/' name '_' args.ng_fun '_' ng_level_str '_' args.nr_fun '_' nr_level_str '.pdf']);

	clear all
end