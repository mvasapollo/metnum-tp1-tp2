bl_size = [4,8,16,32,64,128,256];

im_256 = 'im256';
t_vs_blsz_256 = [100
140
220
370
700
1380
2830];

im_512 = 'im512';
t_vs_blsz_512 = [420
570
880
1480
2770
5360
10950];

im_1024 = 'im1024';
t_vs_blsz_1024 = [1690
2200
3440
6000
11090
21540
43660];

figure;
hold on;
title('Tiempo de ejecucion vs tamano de bloque');
xlabel('tamano (s/u)');
ylabel('t (msecs)');
plot(bl_size,t_vs_blsz_256,'rx-');
plot(bl_size,t_vs_blsz_512,'gx-');
plot(bl_size,t_vs_blsz_1024,'bx-');
legend({im_256, im_512, im_1024});

export_fig('../informe/graficos/t_vs_tambloque.pdf');