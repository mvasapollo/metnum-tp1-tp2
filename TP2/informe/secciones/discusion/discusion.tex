\subsection{Características de las transformadas}

Las figuras introducidas en la sección \ref{sec:res-seniales-transformadas} muestran las características comentadas en la sección \ref{subsec:des-datos} acerca de las transformadas de los distintos tipos de señal.

En la figura (\ref{fig:sen-transf-tipo1}) se observan picos para frecuencias bajas, anulándose la transformada por completo luego del último pico. El pico de mayor magnitud, de valor negativo, corresponde presumiblemente al coseno de frecuencia igual a 2, y el segundo en orden de magnitud, al coseno de frecuencia igual a 7.

En el caso de $x(t) = cos(t^2)$, figura (\ref{fig:sen-transf-tipo2}), se encuentran múltiples picos en las frecuencias bajas, si bien la señal está formada por un único coseno. Por otro lado, se puede percibir un pequeño riple que se extiende hacia frecuencias mayores.

Como se esperaba, la función discontinua de la figura (\ref{fig:sen-transf-tipo3}) muestra efectivamente mucha mayor presencia en el rango superior del espectro (notar que se encuentran picos para frecuencias mucho más elevadas que en los casos anteriores, con magnitudes apreciables).

\subsection{Tipos de ruido}

En la figura (\ref{fig:tipos-ruido-1D}) se aprecia la naturaleza de cada tipo de ruido. En primer lugar, el ruido \emph{aditivo} muestra una perturbación relativamente uniforme sobre toda la señal. El ruido \emph{speckle}, a su vez, produce una perturbación similar a la del ruido \emph{aditivo}, pero afecta más sensiblemente a las zonas de la señal de mayor valor absoluto; se observa cómo los valores cercanos a 0 permanecen prácticamente inalterados, habiendo una perturbación progresivamente mayor a medida que se incrementa el valor de la señal. En la imágenes correspondientes de la figura (\ref{fig:tipos-ruido-2D}), se observan alteraciones similares, aunque la imágen central muestra mayor definición ya que el ruido \emph{speckle} altera en menor medida a los contornos oscuros (de valor cercano a 0).

El tipo de ruido \emph{salt and pepper}, en cambio, agrega picos muy marcados y delgados aleatoriamente a lo largo de la señal. Análogamente, la imagen correspondiente se percibe de un color más grisáceo a la vista que las demás, al mezclarse puntos negros y blancos con el resto del contenido visual.

\subsection{Análisis del rendimiento en señales}

En todos los casos, se halló que existe un valor óptimo de $k$ para el cual se logra el mejor nivel de recuperación en el pico de las curvas en las figuras (\ref{fig:psnr-vs-k-aditivo}) y (\ref{fig:psnr-vs-k-speckle}). En estas configuraciones, se lograron incrementos del PSNR en hasta 10db, mostrando mejoras notables (ver figura \ref{fig:antes-despues-2}). Para el filtrado \emph{umbral} el máximo se encontró siempre en el intervalo $0-0.2$, para el esquema \emph{cuantil} en $0.9-1$, y para el \emph{espectral} en $0.5-0.8$, aunque mostrando mayor variabilidad.

Al comparar la efectividad entre los distintos esquemas, se observa que dada una determinada señal ruidosa de la clase 1 o 2, en todos los métodos se alcanza un PSNR máximo aproximadamente equivalente (por ejemplo, la señal de clase 1 con ruido \emph{aditivo} llega a 34dB en los tres gráficos). Esto pareciera determinar que la señal misma tiene un tope en el nivel de recuperación que se puede alcanzar una vez que se le agregó cierta cantidad de ruido, indiferentemente del método que se utilice. Las señales de la clase 3, sin embargo, muestran una mala disposición para el filtrado \emph{umbral}, logrando siempre un tope menor que para los otros dos esquemas.

Adicionalmente, como se dijo en la sección \ref{subsec:des-comparacion}, en cada caso se partió de una señal ruidosa con un PSNR de aproximadamente 20db respecto a la señal original. Sin embargo, se ve que sistemáticamente el máximo alcanzado por las señales sinusoidales (clase 1) es mayor al de las demás. Similarmente, el de la clase 2 siempre es mayor al de la clase 3. Esto significa, presumiblemente, que cuanto menos afín sea la señal original respecto a la base de representación, menor será el tope de recuperación posible luego de ser perturbada. Análogamente, aquellas señales que tienen mayor contenido en el espectro de frecuencias altas, limitan la efectividad posible en la recuperación.

Los ejemplos en las figuras (\ref{fig:antes-despues-1}) a \ref{fig:antes-despues-3-y} verifican la efectividad del procedimiento, mostrando una señal recuperada que guarda mayor similitud a la original que la versión perturbada.

Por otro lado, los gráficos de las transformadas dejan ver que todos los esquemas de filtrado atacan principamente a las frecuencias altas, sosteniendo lo dicho en la sección \ref{subsec:aplic-reduc-ruido}.

\subsection{Tratamiento de imágenes}

El rendimiento para el caso bidimensional es inferior a lo discutido en la sección anterior. Si bien se lograron incrementos significativos en el PSNR, las configuraciones para ello fueron más difíciles de hallar. Por otro lado, incluso en casos donde se logran grandes mejoras (ver figura \ref{fig:ime-1}), la imagen recuperada siempre pierde definición, y a simple vista resulta más perturbada que la versión previa al filtrado (indicando que posiblemente el PSNR no sea una medida apropiada de la calidad percibida visualmente).
Respecto a la divisón en bloques, esta mostró ser fundamental al procesar imágenes. En primer lugar, como se dijo en la sección (\ref{res:exp-imgns}), no se lograron buenos resultados bajo ninguna configuración sin subdividir la imagen. Al manipular la imagen de forma íntegra, la imagen recuperada se degrada notablemente, al perder fuertemente el contraste y los contornos. Esto es análogo al suavizamiento que se percibe en señales en el entorno de las discontinuidades, como se ve en la figura (\ref{fig:antes-despues-3}).

Por otro lado, el incremento en velocidad es notable; el gráfico en la figura (\ref{fig:t_vs_tambloque}) muestra una relacion marcadamente lineal entre el tamaño de bloque y el tiempo de ejecución.

\subsection{DCT}

La experiencia al procesar imágenes deja entrever que una limitación fundamental del procedimiento es el tamaño de la muestra que se manipula. Cuanto mayor (y más compleja) sea una muestra, más presencia tendrá en el espectro alto en el plano de las frecuencias, ya que la transformada actúa de forma global (no localizada) sobre la muestra. En cambio, el subdividir la muestra y manipularla por bloques, se obtienen muestras que son localmente mejor comportadas (por ejemplo si se dividiera la rampa de la figura \ref{fig:antes-despues-3} en tres secciones lineales). Esto permite una reducción de ruido más efectiva en cada subdivisión, aunque puede producir problemas de cohesión a nivel global, como en la figura (\ref{fig:ime-2}) donde la imagen parece levemente cuadriculada.