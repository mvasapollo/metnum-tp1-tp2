\subsection{Selección de datos de prueba}
\label{subsec:des-datos}

Para la experimentación en el caso unidimensional, se generaron señales de formas variadas y muestras de diferentes tamaños. Ante la diversidad de formas posibles, se optó por experimentar con señales de las siguientes clases: sumas de cosenos de diferentes frecuencias, funciones símil sinusoidales pero con frecuencia variable, y funciones contínuas de a trozos pero con discontinuidades muy marcadas.

La primer clase se utilizó para retratar la forma en que se manifiesta la señal y el ruido en el plano de la frecuencia, esperándose ver picos altamente destacados para las frecuencias de aquellos cosenos que componen la señal, y picos secundarios en frecuencias elevadas.

En el segundo caso, al ser las señales menos afines a la base mediante las cual se las representa, es esperable encontrar que se requiera un mayor espectro de cosenos elementales para formar la señal, precisando de frecuencias mayores.

La tercera clase se incorporó como un caso extremo, ya que para representar las discontinuidades se requieren frecuencias posiblemente mucho más elevadas que las de la señal misma. De esta forma, los picos debidos a componentes ruidosas de la señal se entreveran con aquellos que componen a la señal original, dificultando ampliamente el filtrado. Este caso es de interés ya que las imágenes presentan este mismo comportamiento; cualquier contorno de alto contraste en una imagen será una discontinuidad marcada cuando se lo vea unidimensionalmente.

Las imágenes de prueba fueron extraídas de una base de datos para investigación en procesamiento de señales e imágenes\footnote{Ver referencia [2].} de libre acceso en internet. Todas ellas son imágenes en escala de grises, cuadradas, y con dimensiones de 256, 512 ó 1024 píxeles.

\subsection{Procedimiento}

El procedimiento utilizado para experimentar fue similar en el caso de señales e imágenes. En una primera instancia, a partir del vector o matriz de datos originales, se obtuvo una versión perturbada de los mismos agregándole ruido en la forma que se detalla en la próxima sección. Luego se transformaron los datos al plano de la frecuencia, se filtraron las frecuencias asociadas al ruido (ver sección \ref{subsec:des-filtrado}), y posteriormente se recuperó una versión mejorada de los datos perturbados.

Para medir la efectividad en la recuperación de los datos originales, se computaron y compararon los valores del PSNR para la versión ruidosa y la versión procesada respecto a la original.

Naturalmente, un incremento de la métrica de un caso al otro sería evidencia de que el procedimiento efectivamente produce una mejora sobre los datos perturbados.

Adicionalmente, se incorporó una variante en el caso de las imágenes. Se analizaron los cambios en eficacia y eficiencia al procesar la imagen como una unidad, o al dividirla en pequeños bloques de igual tamaño y realizar el procedimiento descripto para cada bloque independientemente.

\subsection{Generación de ruido}

El agregado de ruido a señales y a imágenes se realizó del mismo modo, experimentándose sobre tres distintos tipos de ruido conocidos como: \emph{aditivo gaussiano}, \emph{speckle} y \emph{salt and pepper}.

El ruido \emph{aditivo} resulta cuando a cada valor de los datos se le suma una perturbación de distribución normal\footnote{Ver apéndice B.}, presentándose en muchas mediciones de fenómenos naturales.

El ruido \emph{speckle} se presenta principalmente en fotografía satelital, y resulta en una perturbación en cada píxel proporcional a su valor de gris. En la práctica, esto se simuló multiplicando cada punto por una variable de distribución gaussiana de media unitaria.

En ambos casos previos, el desvío estándar de la variable utilizada se eligió en función del rango de valores de los datos (a mayor rango, mayor desvío) y de un parámetro de escalamiento, permitiendo una medida uniforme de perturbación para señales e imágenes.

El ruido \emph{salt and pepper} consiste en una aparición aleatoria de puntos blancos o negros en toda la imagen, principalmente como resultado de fallas electrónicas (y por lo tanto, común en cámaras digitales). Para el caso de señales, el concepto de blanco o negro se reemplazó por mínimo valor y máximo valor en magnitud de la señal.

\subsection{Esquemas de filtrado}
\label{subsec:des-filtrado}

Como se dijo en la sección (\ref{subsec:aplic-reduc-ruido}), es necesario identificar y eliminar aquellas frecuencias que representan la componente ruidosa de la señal. Esto implica naturalmente que se debe utilizar algún criterio de valoración para decidir cuáles señales son indeseadas, y cómo eliminarlas. Se experimentó con los siguientes tres criterios en función de un parámetro $k$, estudiando la efectividad para diferentes tipos de ruido y en diferentes señales.

\begin{itemize}
	\item \emph{Umbral}: se anulan todas las frecuencias cuya magnitud no supere $k * max$, donde $max$ es la magnitud máxima de una frecuencia en la transformada.
	\item \emph{Cuantil}: se anula el $k * 100$\% de frecuencias de menor magnitud en la tranformada.
	\item \emph{Espectral}: se computa la suma total de la magnitud de aporte de todas las frecuencias, y luego, comenzando por las frecuencias bajas, se acumula la magnitud de cada frecuencia hasta alcanzar $(1 - k) * total$. A partir de ese punto, todas las frecuencias más altas se anulan.
\end{itemize}

\subsection{Comparación de efectividad en la reducción de ruido}
\label{subsec:des-comparacion}

En una aplicación real, los datos y el tipo y nivel de ruido son parte del problema planteado, siendo desconocido el PSNR respecto a la señal o imagen original. Por esta razón, es de interés determinar cuál esquema de filtrado y valor de parametrización dará mejores resultados ante una instancia particular.

Como primera etapa, se experimentó sobre el caso unidimensional, observando la efectividad para distintos tipos de combinación de clase de señal, tipo de ruido, y esquema de filtrado.

Para obtener resultados comparables entre los distintos casos, se agregó en cada señal un nivel de ruido (variable según el tipo) suficiente para lograr que la versión a procesar tenga un PSNR de aproximadamente 20db respecto a la original. De esta forma, se compara el PSNR obtenido luego de reducir el ruido mediante distintos métodos y niveles de filtrado, y aquel que haya logrado un valor más alto es el de mejor efectividad.