\subsection{Transformada Discreta del Coseno (DCT)}
\label{sec:intro-dct}

La Transformada Discreta del Coseno (o DCT por sus iniciales en inglés), es una versión discreta de la Serie de Cosenos de Fourier\footnote{Ver referencia [1].} (la cual a su vez es un caso particular de la Serie de Fourier). Esta permite expresar una función $x: \left [0, L \right ] \mapsto \mathbb{R} $ como:
$$x(t) = \frac{1}{2} a_0 + \sum_{n=1}^\infty a_n cos\left ( \frac{n \pi}{L} t \right ).$$

Es decir, se representa a $x(t)$ como una suma de infinitos cosenos donde $a_i$ representa la magnitud o el aporte del $i$-ésimo coseno; o equivalentemente, se representa a $x(t)$ en la base $\mathbb{B} = \left \{ 1, cos\left ( \frac{1 \pi}{L} t \right ), cos\left ( \frac{2 \pi}{L} t \right ), ... \right \}$. De la teoría de Fourier, se desprende además que esta base es ortogonal.

En el caso discreto donde únicamente se conocen $N$ valores de $x(t)$, la fórmula previa se puede aproximar utilizando muestras de los primeros $N$ cosenos, y expresando el vector de datos $x \in \mathbb{R}^N$ como una combinación lineal de estas. En particular, si se toman muestras de tamaño $N$, se puede armar una matriz cuadrada $M \in \mathbb{R}^{N \times N}$ donde la $j$-ésima columna es la muestra del $j$-ésimo coseno. Si el muestreo es lineal en el dominio de $x$, estas columnas heredan la propiedad del caso contínuo; son una base ortogonal (de $\mathbb{R}^N$).

De esta forma, los coeficientes $a_i$ pueden ser vistos como los coeficientes del cambio de base de $x$ en la base formada por las columnas de $M$. Llamando a este vector $y$, la operación para obtenerlo es el producto:
\begin{equation}
y = M x .
\label{eqn:transf-1D}
\end{equation}

Inversamente, dado un vector $\hat{y}$ en la base asociada a la matriz $M$, si se desea recuperar el vector de datos, es necesario resolver el sistema:
\begin{equation}
M \hat{x} = \hat{y}.
\label{eqn:antitransf-1D}
\end{equation}

Es común referirse a este procedimiento como transformación de $x$ al plano de la frecuencia, y al inverso como transformación de $y$ al plano del tiempo o antitransformada.

\subsection{El caso bidimensional}

Para utilizar lo anterior en el caso de una imagen en escala de grises, es conveniente pensar en ella como una muestra de una función $X: \mathbb{R} \times \mathbb{R} \mapsto [0,255]$, y darle un tratamiento similar al caso unidimensional. Cuando la función es bidimensional, también es posible representarla en el plano de la frecuencia, aunque en este caso las funciones que componen la base son bivariadas. Estas toman la siguiente forma:
$$cos\left ( \frac{n_1 \pi}{L} x_1 \right ) cos\left ( \frac{n_2 \pi}{L} x_2 \right )$$
con $0 \leq n_1 < \infty$ y $0 \leq n_2 < \infty$.

Para el caso discreto, donde $X \in \mathbb{R}^{N \times N}$ es la matriz de datos, la transformación se extiende de forma sencilla, transformando por filas y columnas mediante la siguiente operación:

\begin{equation}
Y = M X M^T.
\label{eqn:transf-2D}
\end{equation}

En este caso, para antitransformar $Y$, es necesario resolver los siguientes sistemas matriciales:

\begin{equation}
Y = M Z
\label{eqn:antitransf-2D-1}
\end{equation}
\begin{equation}
Z^T = M X^T
\label{eqn:antitransf-2D-2}
\end{equation}

con $Z \in \mathbb{R}^{N \times N}$.

\subsection{Aplicación a la reducción de ruido}
\label{subsec:aplic-reduc-ruido}

Obtener la magnitud del aporte de cada frecuencia en una señal es de gran utilidad cuando se busca reducir el ruido en ella. Varios tipos de ruido que se presentan frecuentemente en el mundo real muestran características similares en el plano de la frecuencia; estos suelen manifestarse como pequeños picos de magnitud en frecuencias altas, lejanas al sector del espectro donde se encuentra el grueso de la señal.
Por esta razón es posible identificar y eliminar las frecuencias que los generan, y reconstruir la señal original prescindiendo del ruido.

\subsection{PSNR}

Como métrica de similitud entre señales o imágenes se utilizó el \emph{Peak Signal-to-Noise Ratio} (traducido como \emph{Relación Señal a Ruido de Pico}), un indicador en decibeles que es independiente de las unidades utilizadas y permite una comparación relativamente uniforme entre muestras de distintos tamaños.

Dada una matriz de datos original $X \in \mathbb{R}^{N \times M}$ y una aproximación ruidosa $\tilde{X}$, se define el PSNR como\footnote{Esta definición también se puede utilizar para vectores tomando $M = 1$.}:

\begin{eqnarray}
PSNR = 10\:log_{10}\left ( \frac{MAX_X^2}{MSE} \right ) \label{eqn:PSNR}\\
MSE = \frac{1}{N M} \sum_{i=0}^{N-1}\sum_{j=0}^{M-1} \left ( X_{ij} - \tilde{X}_{ij} \right )^2
\end{eqnarray}

donde $MAX_X$ es el valor pico de la matriz de datos.

Se observa a partir de la definición que el PSNR es decreciente en función del error cuadrático medio $MSE$, por lo cuál un mayor valor en esta métrica implica mayor similitud entre los datos que se comparan.